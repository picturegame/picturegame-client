import json
import os
import shutil
import sys

outpath = sys.argv[1]

if not os.path.isdir(outpath):
    if os.path.exists(outpath):
        raise Exception(f'File already exists at {outpath}')

    os.mkdir(outpath)

config = {}
version = ''

with open('config.json') as configFile:
    config = json.load(configFile)

with open('VERSION') as versionFile:
    version = versionFile.read().strip()

config['version'] = version

for f in os.listdir(outpath):
    path = os.path.join(outpath, f)
    print(f'$ rm {path}')
    os.remove(path)

distPath = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'dist')
for f in os.listdir(distPath):
    inpath = os.path.join(distPath, f)

    print(f'$ cp {inpath} {outpath}')
    shutil.copy(inpath, outpath)

with open(os.path.join(outpath, 'config.json'), 'w') as outConfig:
    print('write config.json')
    json.dump(config, outConfig)
