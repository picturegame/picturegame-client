import { Round, RoundAggregates } from 'picturegame-api-wrapper';

export type FieldTranslations<T> = Record<keyof T, string>;

export const RoundFieldTranslations: Readonly<FieldTranslations<Round>> = {
    roundNumber: 'Round number',

    title: 'Title',
    postUrl: 'URL',
    thumbnailUrl: 'Thumbnail URL',
    id: 'Post ID',
    winningCommentId: 'Winning comment ID',

    hostName: 'Host',
    postTime: 'Start time',

    winnerName: 'Winner',
    winTime: 'End time',
    plusCorrectTime: '+correct time',
    abandonedTime: 'Abandon time',

    postDelay: 'Post delay',
    solveTime: 'Solve time',
    plusCorrectDelay: '+correct delay',

    postDayOfWeek: 'Start day of week',
    postTimeOfDay: 'Start time of day',
    winDayOfWeek: 'End day of week',
    winTimeOfDay: 'End time of day',

    abandoned: 'Abandoned',
};

export const RoundAggFieldTranslations: Readonly<FieldTranslations<RoundAggregates>> = {
    numRounds: 'Number of rounds',
    numHosts: 'Number of hosts',
    numWinners: 'Number of winners',

    minRoundNumber: 'Min round number',
    maxRoundNumber: 'Max round number',
    allRoundNumbers: 'All round numbers',

    minPostTime: 'Earliest post time',
    maxPostTime: 'Latest post time',

    minWinTime: 'Earliest win time',
    maxWinTime: 'Latest win time',
    allWinTimes: 'All win times',

    avgSolveTime: 'Average solve time',
    minSolveTime: 'Min solve time',
    maxSolveTime: 'Max solve time',

    avgPostDelay: 'Average post delay',
    minPostDelay: 'Min post delay',
    maxPostDelay: 'Max post delay',

    avgPlusCorrectDelay: 'Average +correct delay',
    minPlusCorrectDelay: 'Min +correct delay',
    maxPlusCorrectDelay: 'Max +correct delay',
};
