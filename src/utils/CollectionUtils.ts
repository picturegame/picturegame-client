export function nextOccurence<T>(data: T[], predicate: (value: T) => boolean, startIndex: number) {
    for (let idx: number = startIndex; idx < data.length; ++idx) {
        if (predicate(data[idx])) {
            return idx;
        }
    }

    return data.length;
}
