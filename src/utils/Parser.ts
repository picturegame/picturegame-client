const DateTimeRegex = /^([0-9]{4})-([0-9]{2})-([0-9]{2})(?:T([0-9]{2}):([0-9]{2}):([0-9]{2}))?/;

export function parseDateTime(rawValue?: string | null): Date | null {
    if (!rawValue) {
        return null;
    }

    const match = rawValue.match(DateTimeRegex);
    if (!match) {
        return null;
    }

    const [, yearStr, monthStr, dateStr, hourStr, minStr, secStr] = match;
    return new Date(Date.UTC(
        parseInt(yearStr, 10),
        parseInt(monthStr, 10) - 1,
        parseInt(dateStr, 10),
        hourStr ? parseInt(hourStr, 10) : 0,
        minStr ? parseInt(minStr, 10) : 0,
        secStr ? parseInt(secStr, 10) : 0,
    ));
}
