import * as Reselect from 'reselect';

import { parseDateTime } from './Parser';

export function moveToStartOfDay(date: Date) {
    date.setUTCHours(0, 0, 0, 0);
}

export function moveToStartOfWeek(date: Date) {
    moveToStartOfDay(date);

    // Start of week is monday so we want day - 1
    // do + 6 instead so we don't get a negative value if it's sunday
    const daysSinceStartOfWeek = (date.getUTCDay() + 6) % 7;
    date.setUTCDate(date.getUTCDate() - daysSinceStartOfWeek);
}

export function moveToStartOfMonth(date: Date) {
    moveToStartOfDay(date);
    date.setUTCDate(1);
}

export function moveToStartOfYear(date: Date) {
    moveToStartOfMonth(date);
    date.setUTCMonth(0);
}

export function addDays(date: Date, days: number) {
    date.setUTCDate(date.getUTCDate() + days);
    return date;
}

export function addMonths(date: Date, months: number) {
    date.setUTCMonth(date.getUTCMonth() + months);
    return date;
}

export function addYears(date: Date, years: number) {
    date.setUTCFullYear(date.getUTCFullYear() + years);
    return date;
}

export function getTodayIsoString() {
    const date = new Date();
    const month = (date.getUTCMonth() + 1).toString().padStart(2, '0');
    const dayOfMonth = date.getUTCDate().toString().padStart(2, '0');
    return `${date.getUTCFullYear()}-${month}-${dayOfMonth}T00:00`;
}

export enum PreCannedDateRange {
    Abs_Today = 'abs_today',
    Abs_ThisWeek = 'abs_this-week',
    Abs_ThisMonth = 'abs_this-month',
    Abs_ThisYear = 'abs_this-year',

    Abs_Yesterday = 'abs_yesterday',
    Abs_LastWeek = 'abs_last-week',
    Abs_LastMonth = 'abs_last-month',
    Abs_LastYear = 'abs_last-year',

    Rel_Day = 'rel_day',
    Rel_Week = 'rel_week',
    Rel_30Day = 'rel_30day',
    Rel_60Day = 'rel_60day',
    Rel_90Day = 'rel_90day',

    Custom = 'custom',
}

export interface DateRangeProps {
    rangeType?: string | null;
    customStart?: string | null;
    customEnd?: string | null;
}

export type DateRange = [Date | null, Date | null];

export const getDateRange: (props: DateRangeProps) => DateRange = Reselect.createSelector(
    (props: DateRangeProps) => props.rangeType,
    (props: DateRangeProps) => props.customStart,
    (props: DateRangeProps) => props.customEnd,
    (rangeType, customStart, customEnd): [Date | null, Date | null] => {
        const now = new Date();

        if (!rangeType) {
            // All time
            return [null, null];
        }

        if (rangeType === PreCannedDateRange.Custom) {
            return [parseDateTime(customStart), parseDateTime(customEnd)];
        }

        let start: Date;

        switch (rangeType) {
            case PreCannedDateRange.Abs_Today:
                moveToStartOfDay(now);
                return [now, null];
            case PreCannedDateRange.Abs_ThisWeek:
                moveToStartOfWeek(now);
                return [now, null];
            case PreCannedDateRange.Abs_ThisMonth:
                moveToStartOfMonth(now);
                return [now, null];
            case PreCannedDateRange.Abs_ThisYear:
                moveToStartOfYear(now);
                return [now, null];

            case PreCannedDateRange.Abs_Yesterday:
                moveToStartOfDay(now);
                start = new Date(Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate() - 1));
                return [start, now];
            case PreCannedDateRange.Abs_LastWeek:
                moveToStartOfWeek(now);
                start = new Date(Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate() - 7));
                return [start, now];
            case PreCannedDateRange.Abs_LastMonth:
                moveToStartOfMonth(now);
                start = new Date(Date.UTC(now.getUTCFullYear(), now.getUTCMonth() - 1, 1));
                return [start, now];
            case PreCannedDateRange.Abs_LastYear:
                moveToStartOfYear(now);
                start = new Date(Date.UTC(now.getUTCFullYear() - 1, 0, 1));
                return [start, now];

            case PreCannedDateRange.Rel_Day:
                now.setUTCDate(now.getUTCDate() - 1);
                return [now, null];
            case PreCannedDateRange.Rel_Week:
                now.setUTCDate(now.getUTCDate() - 7);
                return [now, null];
            case PreCannedDateRange.Rel_30Day:
                now.setUTCDate(now.getUTCDate() - 30);
                return [now, null];
            case PreCannedDateRange.Rel_60Day:
                now.setUTCDate(now.getUTCDate() - 60);
                return [now, null];
            case PreCannedDateRange.Rel_90Day:
                now.setUTCDate(now.getUTCDate() - 90);
                return [now, null];

            default:
                return [null, null];
        }
    });

export function convertToLeaderboardFilter([rangeStart, rangeEnd]: DateRange): string | undefined {
    if (rangeStart === null && rangeEnd === null) {
        return undefined;
    }

    const components: string[] = [];
    if (rangeStart !== null) {
        components.push(`winTime gte ${rangeStart.toISOString()}`);
    }
    if (rangeEnd !== null) {
        components.push(`winTime lt ${rangeEnd.toISOString()}`);
    }

    if (components.length === 1) {
        return components[0];
    }
    return `(${components[0]} and ${components[1]})`;
}
