import * as _ from 'lodash-es';

const Months = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
];

function componentise(totalSeconds: number) {
    const seconds = totalSeconds % 60;
    const totalMinutes = Math.floor(totalSeconds / 60);
    const minutes = Math.floor(totalMinutes % 60);
    const totalHours = Math.floor(totalMinutes / 60);

    return [totalHours, minutes, seconds];
}

export function formatDuration(totalSeconds?: number) {
    if (_.isNil(totalSeconds)) {
        return totalSeconds;
    }

    const [totalHours, minutes, seconds] = componentise(totalSeconds);
    const components: string[] = [];

    if (totalHours) {
        components.push(`${totalHours}h`);
    }
    if (minutes) {
        components.push(`${minutes}m`);
    }
    if ((seconds && !totalHours) || components.length === 0) {
        components.push(`${Math.floor(seconds)}s`);
    }

    return components.join(' ');
}

export function formatTimeDiff(from?: number, to?: number) {
    if (_.isNil(from) || _.isNil(to)) {
        return undefined;
    }
    return formatDuration(to - from);
}

function toDate(timestamp: number | string | Date) {
    return typeof timestamp === 'number' ? new Date(timestamp * 1000)
        : typeof timestamp === 'string' ? new Date(timestamp)
            : timestamp;
}

export function formatDate(timestamp?: number | string | Date | null) {
    if (_.isNil(timestamp)) {
        return timestamp;
    }

    const date = toDate(timestamp);
    const month = Months[date.getUTCMonth()];
    return `${date.getUTCDate()} ${month} ${date.getUTCFullYear()}`;
}

export function formatMonth(timestamp?: number | string | Date) {
    if (_.isNil(timestamp)) {
        return timestamp;
    }

    const date = toDate(timestamp);
    return Months[date.getUTCMonth()];
}

export function formatMonthYear(timestamp?: number | string | Date) {
    if (_.isNil(timestamp)) {
        return timestamp;
    }

    const date = toDate(timestamp);
    return `${Months[date.getUTCMonth()]} ${date.getUTCFullYear()}`;
}

export function formatYear(timestamp?: number | string | Date) {
    if (_.isNil(timestamp)) {
        return timestamp;
    }

    return `${toDate(timestamp).getUTCFullYear()}`;
}

export function formatDateTime(timestamp?: number) {
    if (_.isNil(timestamp)) {
        return timestamp;
    }

    return new Date(timestamp * 1000).toUTCString();
}

export function formatDateTimeShort(timestamp?: number | Date) {
    if (_.isNil(timestamp)) {
        return timestamp;
    }

    const date = typeof timestamp === 'number' ? new Date(timestamp * 1000) : timestamp;
    const hour = date.getUTCHours();
    const minute = date.getUTCMinutes().toString().padStart(2, '0');
    const amPm = hour < 12 ? 'AM' : 'PM';
    return `${formatDate(date)} ${hour % 12}:${minute} ${amPm}`;
}

export function formatTimeOfDay(timeSeconds: number) {
    const [totalHours, minutes] = componentise(timeSeconds);
    const amPm = totalHours < 12 ? 'AM' : 'PM';

    let hour = totalHours % 12;
    if (hour === 0) {
        hour = 12;
    }

    return `${hour}:${minutes.toString().padStart(2, '0')} ${amPm}`;
}
