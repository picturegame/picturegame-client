export interface StringMatch {
    value: string;
    score: number;
    tokens: MatchToken[];
}

interface MatchToken {
    value: string;
    matching: boolean;
}

/**
 * Get a score based on how well a search string matches a test string.
 * Favours exact prefixes over fuzzy/subsequence matches
 * All chars in searchString must appear in testString, in order.
 */
export function getStringMatch(testString: string, searchString: string, maxValueLength: number): StringMatch | undefined {
    const testStringLower = testString.toLowerCase();
    let score = 0;
    const tokens: MatchToken[] = [];

    if (!searchString || searchString.length > testString.length) {
        return;
    }

    const substringMatch = testStringLower.indexOf(searchString);
    if (substringMatch !== -1) {
        const numUnmatchedAfter = testString.length - substringMatch - searchString.length;

        score = (maxValueLength - substringMatch) * searchString.length * searchString.length + (1 / (numUnmatchedAfter + 1));

        if (substringMatch > 0) {
            tokens.push({ value: testString.substring(0, substringMatch), matching: false });
        }

        tokens.push({ value: testString.substr(substringMatch, searchString.length), matching: true });

        if (substringMatch + searchString.length < testString.length) {
            tokens.push({ value: testString.substring(substringMatch + searchString.length), matching: false });
        }

        return { score, tokens, value: testString };
    }

    if (searchString.length === 1) {
        // Single-character searchString - there's no concept of "fuzzy" matching
        return;
    }

    let currentToken = '';
    let matching = false;

    function add(char: string, isMatch: boolean) {
        if (matching === isMatch) {
            currentToken += char;
            return;
        }
        if (currentToken) {
            tokens.push({ matching, value: currentToken });
            if (matching) {
                score += currentToken.length * currentToken.length / (numCharsSkipped + 1);
            }
        }
        currentToken = char;
        matching = isMatch;
    }

    let tIdx = 0;
    let numCharsSkipped = 0;
    for (const sChar of searchString) {
        let found = false;
        while (tIdx < testString.length) {
            const tChar = testStringLower[tIdx];
            const tCharOrig = testString[tIdx];
            tIdx++;

            if (sChar === tChar) {
                add(tCharOrig, true);
                found = true;
                break;
            }

            add(tCharOrig, false);
            numCharsSkipped++;
        }
        if (!found) {
            // Reached the end of the test string but there are still more search chars to go
            return;
        }
    }

    // Add a bonus for proximity to the end of the test string
    numCharsSkipped += testString.length - tIdx;
    score += 1 / (numCharsSkipped + 1);

    // Add remainder of the string to the tokenised list
    if (currentToken) {
        tokens.push({ matching: true, value: currentToken });
    }
    tokens.push({ matching: false, value: testString.slice(tIdx) });

    return {
        score,
        tokens,
        value: testString,
    };
}
