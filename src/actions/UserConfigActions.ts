import { ChartId, ThemeSetting, UserConfig } from '../model/ui';
import { createActionCreator } from '../state/Actions';

export const SetTheme = createActionCreator<ThemeSetting>('UserConfig.SetTheme');

export const ReplaceUserConfig = createActionCreator<UserConfig | null>('UserConfig.ReplaceUserConfig');

export interface SetPlayerColourArgs {
    username: string;
    colour: string;
}
export const SetPlayerColour = createActionCreator<SetPlayerColourArgs>('UserConfig.SetPlayerColour');

export const SetHideWeeklyWinsMovingAverage = createActionCreator<boolean>('UserConfig.SetHighWeeklyWinsMovingAverage');

export const ToggleChartVisibility = createActionCreator<ChartId>('UserConfig.ToggleChartVisibility');
