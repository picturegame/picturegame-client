import * as UI from '../model/ui';
import { createActionCreator } from '../state/Actions';

export const AddPopup = createActionCreator<UI.PopupMessage>('View.AddPopup');

export interface DeletePopupArgs {
    id: string;
}
export const DeletePopup = createActionCreator<DeletePopupArgs>('View.DeletePopup');
