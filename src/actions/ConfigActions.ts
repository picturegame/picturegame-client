import * as UI from '../model/ui';
import { createActionCreator } from '../state/Actions';

export const SetConfig = createActionCreator<UI.Config>('Config.SetConfig');
