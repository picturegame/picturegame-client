import * as API from 'picturegame-api-wrapper';
import * as React from 'react';

import { UpdateLeaderboard } from '../actions/DataActions';
import { AddPopup } from '../actions/ViewActions';

import { RoundQuery } from '../model/api';
import * as UI from '../model/ui';

import { delay } from '../utils/PromiseUtils';

import { Endpoint } from './Endpoint';

const NetworkErrorRetryDelayMs = 15000;
const RateLimitRetryDelayMs = 15000;

export const ApiContext = React.createContext<PicturegameApi>(null as any);

export class ApiError extends Error {
    constructor(public status?: number) {
        super('Unexpected response from the PictureGame API');
        Object.setPrototypeOf(this, ApiError.prototype);
    }
}

interface ApiFetchArgs {
    path: Endpoint;
    query?: any;
    expectedStatus?: number;
    method?: string;
    body?: object;
}

export class PicturegameApi {
    private _popupId: number = 0;

    private _lastLeaderboardFetch = 0;

    constructor(public readonly url: string, private dispatch: UI.DispatchAction) {
    }

    public async getRoundListResponse(query?: RoundQuery) {
        return await this.apiFetch({ path: Endpoint.Rounds, query }) as API.ListResponse<API.Round>;
    }

    public async getSinglePageRounds(query?: RoundQuery) {
        return (await this.getRoundListResponse(query)).results;
    }

    public async getPlayerMetrics(names: string[], filterRounds?: string) {
        const results: API.Player[] = [];

        let offset = 0;
        let response: API.ListResponse<API.Player>;

        do {
            const query: any = { names, filterRounds };
            if (offset) {
                query.offset = offset;
            }
            response = await this.apiFetch({ path: Endpoint.Players, query }) as API.ListResponse<API.Player>;
            for (const result of response.results) {
                results.push(result);
            }
            offset += response.pageSize;
        } while (offset < response.totalNumResults);

        return results;
    }

    public async getLeaderboard(filter?: string) {
        const currentFetch = ++this._lastLeaderboardFetch;

        const query: any = {
            filterRounds: filter,
            includeWinTimes: true,
        };
        const response = await this.apiFetch({ path: Endpoint.Leaderboard, query }) as API.LeaderboardResponse;

        if (this._lastLeaderboardFetch === currentFetch) {
            this.dispatch(UpdateLeaderboard({ players: response.leaderboard }));
        }
    }

    public async bulkRequest(request: API.BulkRequest): Promise<API.BulkResponse> {
        return await this.apiFetch({
            path: Endpoint.Bulk,
            body: request,
            expectedStatus: 200,
            method: 'POST',
        });
    }

    private async apiFetch(args: ApiFetchArgs): Promise<any> {
        const queryParams = getQueryParams(args.query);

        let url = `${this.url}/${args.path}`;
        if (queryParams) {
            url += `?${queryParams}`;
        }

        const fetchArgs: RequestInit = { method: args.method ?? 'GET' };
        if (args.body) {
            fetchArgs.body = JSON.stringify(args.body);
            fetchArgs.headers = { 'Content-Type': 'application/json' };
        }

        async function send() {
            const res = await fetch(url, fetchArgs);
            if (res.status !== (args.expectedStatus ?? 200)) {
                throw new ApiError(res.status);
            }
            return await res.json();
        }

        while (true) {
            try {
                return await send();

            } catch (e) {
                if (e instanceof TypeError) {
                    console.warn('Network Error, retrying in 15 seconds');
                    this.dispatch(AddPopup({
                        id: `${this._popupId++}`,
                        message: 'Connection error - retrying in 15 seconds...',
                        type: 'error',
                        timeout: NetworkErrorRetryDelayMs - 500,
                    }));
                    await delay(NetworkErrorRetryDelayMs);
                    continue;
                }
                if (e instanceof ApiError) {
                    if (e.status === 429) {
                        console.warn('429 response returned from the API, retrying in 15 seconds');
                        this.dispatch(AddPopup({
                            id: `${this._popupId++}`,
                            message: "You're doing that too much - retrying in 15 seconds...",
                            type: 'error',
                            timeout: RateLimitRetryDelayMs - 500,
                        }));
                        await delay(RateLimitRetryDelayMs);
                        continue;
                    }
                    if (e.status === 400) {
                        // Bad request - not necessarily "something went wrong" territory; let the caller handle it
                        throw e;
                    }
                }
                this.dispatch(AddPopup({
                    id: `${this._popupId++}`,
                    message: 'An unknown error occurred - please contact Provium if this continues',
                    type: 'error',
                    dismissable: true,
                }));
                console.error(e, e.stack);
                throw e;
            }
        }
    }
}

function getQueryParams(queryObj: any = {}) {
    const query = new URLSearchParams();
    for (const k in queryObj) {
        const param = formatQueryParam(queryObj[k]);
        if (param) {
            query.set(k, param);
        }
    }

    return query.toString();
}

function formatQueryParam(param: any): string | undefined {
    if (typeof param === 'string') {
        return param;
    }
    if (typeof param === 'number' || param === null || typeof param === 'boolean') {
        return `${param}`;
    }
    if (param instanceof Date) {
        return param.toISOString();
    }
    if (Array.isArray(param)) {
        const params: string[] = [];
        for (const p of param) {
            const formatted = formatQueryParam(p);
            if (formatted) {
                params.push(formatted);
            }
        }
        return params.join(',');
    }

    return undefined;
}
