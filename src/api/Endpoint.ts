export const enum Endpoint {
    Leaderboard = 'leaderboard',
    Players = 'players',
    Rounds = 'rounds',
    RoundsAggregate = 'rounds/aggregate',
    Bulk = 'bulk',
}
