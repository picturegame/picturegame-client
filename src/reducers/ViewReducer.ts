import * as ViewActions from '../actions/ViewActions';

import * as UI from '../model/ui';

import { Reducer } from '../state/Reducer';

export function init(initialState: UI.ViewState) {
    const reducer = new Reducer(initialState);

    reducer.handle(ViewActions.AddPopup, (viewState, newPopup): UI.ViewState => {
        return {
            ...viewState,
            popups: [
                ...viewState.popups,
                newPopup,
            ],
        };
    });

    reducer.handle(ViewActions.DeletePopup, (viewState, { id }): UI.ViewState => {
        return {
            ...viewState,
            popups: viewState.popups.filter(p => p.id !== id),
        };
    });

    return reducer;
}
