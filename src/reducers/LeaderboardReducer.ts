import * as DataActions from '../actions/DataActions';
import * as LeaderboardActions from '../actions/LeaderboardActions';

import * as UI from '../model/ui';

import { Reducer } from '../state/Reducer';

export function init(initialState: UI.LeaderboardState) {
    const reducer = new Reducer(initialState);

    reducer.handle(LeaderboardActions.SetJumpedPlayer, (lbState, jumpedPlayer): UI.LeaderboardState => ({
        ...lbState,
        jumpedPlayer,
    }));

    reducer.handle(DataActions.ClearPlayerData, (lbState): UI.LeaderboardState => ({
        ...lbState,
        dataLoaded: false,
    }));

    reducer.handle(DataActions.UpdateLeaderboard, (lbState): UI.LeaderboardState => ({
        ...lbState,
        dataLoaded: true,
    }));

    return reducer;
}
