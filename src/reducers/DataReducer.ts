import * as DataActions from '../actions/DataActions';

import * as UI from '../model/ui';

import { Reducer } from '../state/Reducer';

export function init(initialState: UI.DataState) {
    const reducer = new Reducer(initialState);

    reducer.handle(DataActions.UpdateLeaderboard, (dataState, args): UI.DataState => {
        const newLeaderboard = dataState.leaderboard.withMutations(lb => {
            for (const player of args.players) {
                lb.set(player.username.toLowerCase(), player);
            }
        });

        return {
            ...dataState,
            leaderboard: newLeaderboard,
        };
    });

    reducer.handle(DataActions.ClearPlayerData, (dataState): UI.DataState => {
        return {
            ...dataState,
            leaderboard: dataState.leaderboard.clear(),
            playerMetrics: dataState.playerMetrics.clear(),
        };
    });

    reducer.handle(DataActions.UpdatePlayerMetrics, (dataState, args): UI.DataState => {
        const newMetrics = dataState.playerMetrics.asMutable();
        const nameSet = new Set(args.names.map(n => n.toLowerCase()));

        for (const player of args.players) {
            const nameLower = player.username.toLowerCase();
            if (dataState.leaderboard.has(nameLower)) {
                newMetrics.set(nameLower, player.stats);
                nameSet.delete(nameLower);
            }
        }

        // Any names that didn't resolve in the request get filled with skeleton aggs to avoid re-fetching
        // This should never occur, but just in case...
        for (const nameLower of nameSet) {
            if (dataState.leaderboard.has(nameLower) && !newMetrics.has(nameLower)) {
                newMetrics.set(nameLower, {
                    avgSolveTime: 0,
                    maxSolveTime: 0,
                    minSolveTime: 0,
                    numRoundsHosted: 0,
                    numRoundsWon: 0,
                });
            }
        }

        return {
            ...dataState,
            playerMetrics: newMetrics.asImmutable(),
        };
    });

    return reducer;
}
