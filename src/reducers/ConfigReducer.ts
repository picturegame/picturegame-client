import * as ConfigActions from '../actions/ConfigActions';

import * as UI from '../model/ui';

import { Reducer } from '../state/Reducer';

export function init(initialState: UI.Config) {
    const reducer = new Reducer(initialState);

    reducer.handle(ConfigActions.SetConfig, (config, newConfig) => {
        return {
            ...config,
            ...newConfig,
        };
    });

    return reducer;
}
