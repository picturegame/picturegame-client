import * as UserConfigActions from '../actions/UserConfigActions';

import * as UI from '../model/ui';

import { Reducer } from '../state/Reducer';

export function init(initialState: UI.UserConfig) {
    const reducer = new Reducer(initialState);

    reducer.handle(UserConfigActions.SetTheme, (userConfig, newTheme): UI.UserConfig => ({
        ...userConfig,
        theme: newTheme,
    }));

    reducer.handle(UserConfigActions.ReplaceUserConfig, (_, newConfig): UI.UserConfig => {
        return newConfig ?? {
            playerColours: {},
            weeklyWinsHideMovingAverage: false,
            chartVisibility: {
                ranks: true,
                rounds: true,
                weekly_wins: true,
            },
        };
    });

    reducer.handle(UserConfigActions.SetPlayerColour, (userConfig, { username, colour }): UI.UserConfig => ({
        ...userConfig,
        playerColours: {
            ...userConfig.playerColours,
            [username]: colour,
        },
    }));

    reducer.handle(UserConfigActions.SetHideWeeklyWinsMovingAverage, (userConfig, value): UI.UserConfig => ({
        ...userConfig,
        weeklyWinsHideMovingAverage: value,
    }));

    reducer.handle(UserConfigActions.ToggleChartVisibility, (userConfig, chartId): UI.UserConfig => ({
        ...userConfig,
        chartVisibility: {
            ...userConfig.chartVisibility,
            [chartId]: !userConfig.chartVisibility[chartId],
        },
    }));

    return reducer;
}
