import * as API from 'picturegame-api-wrapper';
import * as React from 'react';

import { UpdatePlayerMetrics } from '../../actions/DataActions';
import { SetPlayerColour } from '../../actions/UserConfigActions';

import { fetchPlayerMetrics } from '../../api/Fetchers';
import { useApiQueryContext } from '../../hooks/Api';
import { useDateRange } from '../../hooks/Misc';
import { useDispatch, useState } from '../../hooks/Redux';
import { getColour } from '../../utils/CssUtils';
import { convertToLeaderboardFilter } from '../../utils/DateUtils';
import { formatDuration } from '../../utils/Formatter';
import { formatRankPeakDates, getPlayerData } from '../../utils/LeaderboardUtils';

import * as UI from '../../model/ui';

import { ColourPicker } from '../base/ColourPicker';
import { buildResolved, CellRenderer, ColumnSpec, DataGrid } from '../base/DataGrid';
import { Link } from '../base/Router';
import { PlayerSearch } from '../misc/PlayerSearch';

const columns: ColumnSpec<UI.Player>[] = [
    {
        id: 'name',
        name: 'Name',
        cell: () => <div />, // Overridden in cellOverrides
        width: 240,
    },
    {
        id: 'rank',
        name: 'Rank',
        cell: p => <td className='col-numeric'>{p.player.rank}</td>,
        width: 60,
    },
    {
        id: 'peakRank',
        name: 'Peak Rank',
        cell: () => <div />, // Overridden in cellOverrides
        width: 70,
    },
    {
        id: 'numWins',
        name: 'Num Wins',
        cell: p => <td className='col-numeric'>{p.player.numWins}</td>,
        width: 60,
    },
    {
        id: 'avgSolveTime',
        name: 'Avg Solve Time',
        cell: buildResolved(p => p.metrics, a => <td>{formatDuration(a.avgSolveTime)}</td>),
        width: 100,
    },
    {
        id: 'minSolveTime',
        name: 'Min Solve Time',
        cell: buildResolved(p => p.metrics, a => <td>{formatDuration(a.minSolveTime)}</td>),
        width: 100,
    },
    {
        id: 'maxSolveTime',
        name: 'Max Solve Time',
        cell: buildResolved(p => p.metrics, a => <td>{formatDuration(a.maxSolveTime)}</td>),
        width: 100,
    },
    {
        id: 'avgPostDelay',
        name: 'Avg Post Delay',
        cell: buildResolved(p => p.metrics, a => <td>{formatDuration(a.avgPostDelay)}</td>),
        width: 100,
    },
    {
        id: 'minPostDelay',
        name: 'Min Post Delay',
        cell: buildResolved(p => p.metrics, a => <td>{formatDuration(a.minPostDelay)}</td>),
        width: 100,
    },
    {
        id: 'maxPostDelay',
        name: 'Max Post Delay',
        cell: buildResolved(p => p.metrics, a => <td>{formatDuration(a.maxPostDelay)}</td>),
        width: 100,
    },
    {
        id: 'avgRoundLength',
        name: 'Avg Round Length',
        cell: buildResolved(p => p.metrics, a => <td>{formatDuration(a.avgRoundLength)}</td>),
        width: 100,
    },
    {
        id: 'minRoundLength',
        name: 'Min Round Length',
        cell: buildResolved(p => p.metrics, a => <td>{formatDuration(a.minRoundLength)}</td>),
        width: 100,
    },
    {
        id: 'maxRoundLength',
        name: 'Max Round Length',
        cell: buildResolved(p => p.metrics, a => <td>{formatDuration(a.maxRoundLength)}</td>),
        width: 100,
    },
];

interface UsernameCellProps {
    player: UI.Player;
    colour: string;
    setColour: (colour: string) => void;
    onRemove?: () => void;
}
function UsernameCell(props: UsernameCellProps) {
    const player = props.player.player;

    return (
        <td className='compare-username-cell'>
            <ColourPicker
                colour={props.colour}
                setColour={props.setColour}
            />
            <div className='username-text'>
                <Link path='/dashboard' query={{ player: player.username }}>
                    {lp => <a {...lp}>{player.username}</a>}
                </Link>
            </div>
            {props.onRemove && <i className='remove-user-button fas fa-times' onClick={props.onRemove} />}
        </td>
    );
}

interface AddPlayerCellProps {
    onAdd: (username: string) => void;
}
function AddPlayerRow(props: AddPlayerCellProps) {
    return (
        <tr className='add-player-row'>
            <td style={{ gridColumn: '1 / -1' }}>
                <PlayerSearch
                    placeholder='Compare with another player'
                    onSubmit={props.onAdd}
                    clearOnSubmit
                />
            </td>
        </tr>
    );
}

interface Props {
    activePlayers: API.LeaderboardEntry[];
    addActivePlayer: (username: string) => void;
    removeActivePlayer: (username: string) => void;
}

function extractDispatch(dispatch: UI.DispatchAction) {
    return {
        onPlayerMetrics: (names: string[], metrics: API.Player[]) =>
            dispatch(UpdatePlayerMetrics({ names, players: metrics })),
        setPlayerColour: (username: string, colour: string) => dispatch(SetPlayerColour({ username, colour })),
    };
}

function extractState(state: UI.State, activePlayers: API.LeaderboardEntry[]) {
    return {
        colours: state.userConfig.playerColours,
        playerData: getPlayerData(state, activePlayers),
    };
}

export function OverviewTable(props: Props) {
    const { activePlayers } = props;
    const { onPlayerMetrics, setPlayerColour } = useDispatch(extractDispatch);
    const { colours, playerData } = useState(extractState, activePlayers);

    const dateRange = useDateRange();
    const filterString = convertToLeaderboardFilter(dateRange);

    const playerMetricsContext = useApiQueryContext(fetchPlayerMetrics, onPlayerMetrics, filterString);

    React.useEffect(() => {
        const fetchNames = playerData.filter(p => !p.metrics).map(p => p.player.username);

        if (fetchNames.length) {
            playerMetricsContext.enqueue(fetchNames);
        }
    }, [playerData, playerMetricsContext]);

    const cellOverrides: Record<string, CellRenderer<UI.Player>> = {
        'name': (p, i) => <UsernameCell
            player={p}
            colour={colours[p.player.username] ?? getColour(i)}
            setColour={colour => setPlayerColour(p.player.username, colour)}
            onRemove={props.activePlayers.length > 1 ? (() => props.removeActivePlayer(p.player.username)) : undefined}
        />,
        'peakRank': p => <td className='col-numeric' title={formatRankPeakDates(p.rankPeaks, dateRange[1])}>
            {p.rankPeaks[0].rank}
        </td>,
    };

    const data: (UI.Player | undefined)[] = [...playerData, undefined];

    return (
        <div className='compare-overview-table'>
            <DataGrid
                heading='Overview'
                columns={columns}
                data={data}
                rowRenderer={(p, c) => <tr key={p.player.username}>{c}</tr>}
                fallbackRow={() => <AddPlayerRow key='$addPlayer' onAdd={props.addActivePlayer} />}
                cellOverrides={cellOverrides}
            />
        </div>
    );
}
