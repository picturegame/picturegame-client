import * as React from 'react';

import { SetJumpedPlayer } from '../../actions/LeaderboardActions';

import { useQueryString, QueryParam } from '../../hooks/History';
import { useDispatch, useState } from '../../hooks/Redux';

import * as UI from '../../model/ui';

import { Button } from '../base/Button';
import { Link } from '../base/Router';
import { PlayerSearch } from '../misc/PlayerSearch';

function getActiveUser(state: UI.State, username: string) {
    return state.data.leaderboard.get(username?.toLowerCase());
}

export function DashboardToolbarModule() {
    const [username, setUsername] = useQueryString(QueryParam.Player);
    const user = useState(getActiveUser, username);

    const viewUsername = user?.username ?? username;

    const label = viewUsername ? `Stats for ${viewUsername}` : 'No player selected';

    return <>
        <span className='tb-module-item tb-module-title'>{label}</span>
        <PlayerSearch
            className='tb-module-item'
            placeholder='Search for player'
            onSubmit={setUsername}
        />
        {user && <Link path='/compare' query={{ 'players': [viewUsername] }}>
            {props =>
                <Button className='tb-module-item' {...props}>
                    <i className='left-icon fas fa-chart-line' />
                    <span className='icon-button-label'>Compare</span>
                </Button>
            }
        </Link>}
    </>;
}

function getSetJumpedPlayer(dispatch: UI.DispatchAction) {
    return (username: string) => dispatch(SetJumpedPlayer(username));
}

function toolbarExtractState(state: UI.State) {
    return {
        players: state.data.leaderboard,
        playersPerPage: state.leaderboard.itemsPerPage,
    };
}

export function LeaderboardToolbarModule() {
    const { players, playersPerPage } = useState(toolbarExtractState);
    const setJumpedPlayer = useDispatch(getSetJumpedPlayer);

    const [, setPage] = useQueryString(QueryParam.Page);

    function onJumpToPlayer(username: string) {
        const player = players.get(username);
        if (!player) {
            return;
        }

        const pageForPlayer = Math.floor((player.rank! - 1) / playersPerPage);
        setPage(pageForPlayer);
        setJumpedPlayer(username);
    }

    return <>
        <span className='tb-module-item tb-module-title'>Leaderboard</span>
        <PlayerSearch
            className='tb-module-item'
            onSubmit={onJumpToPlayer}
            placeholder='Jump to player'
        />
    </>;
}

export function CompareToolbarModule() {
    return <span className='tb-module-item tb-module-title'>Compare</span>;
}

export function RoundsToolbarModule() {
    return <span className='tb-module-item tb-module-title'>Explore Rounds</span>;
}

export function FactsAndFiguresToolbarModule() {
    return <span className='tb-module-item tb-module-title'>Facts and Figures 2022</span>;
}
