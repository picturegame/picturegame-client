import * as React from 'react';

import { classNameFromObj } from '../../utils/CssUtils';

import { Button } from '../base/Button';
import { Link, Route, Router } from '../base/Router';

import {
    CompareToolbarModule,
    DashboardToolbarModule,
    FactsAndFiguresToolbarModule,
    LeaderboardToolbarModule,
    RoundsToolbarModule,
} from './Modules';

import './Toolbar.scss';

import snoo from '../../../static/snoo.png';

interface ToolbarProps {
    onOpenSidebar: () => void;
}

export function Toolbar(props: ToolbarProps) {
    const [showModules, setShowModules] = React.useState(false);

    const className = classNameFromObj({
        'app-toolbar': true,
        'tb-modules-shown': showModules,
    });

    function onOpenSidebar(ev: React.MouseEvent<HTMLDivElement>) {
        ev.preventDefault();
        props.onOpenSidebar();
    }

    return (<div className={className}>
        <div className='toolbar-block-left'>
            <Button onClick={onOpenSidebar}>
                <i className='fas fa-bars' />
            </Button>
            <Link path='/'>
                {p => <a {...p} >
                    <img className='snoo-icon' src={snoo} />
                </a>}
            </Link>
            <div className='tb-module-route'>
                <RouteModule />
            </div>
        </div>
        <div className='toolbar-block-right'>
            {renderModulesToggle()}
        </div>
    </div>);

    function renderModulesToggle() {
        const icon = showModules ? 'chevron-right' : 'ellipsis-h';
        return <Button className='tb-modules-toggle' onClick={() => setShowModules(!showModules)}>
            <i className={`fas fa-${icon}`} />
        </Button>;
    }
}

function RouteModule() {
    return (
        <Router>
            <Route path='/'><LeaderboardToolbarModule /></Route>
            <Route path='/dashboard'><DashboardToolbarModule /></Route>
            <Route path='/compare'><CompareToolbarModule /></Route>
            <Route path='/rounds'><RoundsToolbarModule /></Route>
            <Route path='/facts'><FactsAndFiguresToolbarModule /></Route>
        </Router>
    );
}
