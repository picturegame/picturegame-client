import * as React from 'react';

import { Button } from './Button';

import { combineProps, ClassStyleProps } from '../../utils/CssUtils';

import './DataContainer.scss';

export interface DataContainerProps extends Partial<PageButtonsProps> {
    heading: React.ReactChild;
    hideBottomPageButtons?: boolean;
    children?: React.ReactNode;
}

export function DataContainer(props: DataContainerProps) {
    const paginated = typeof props.currentPage === 'number'
        && props.onChangePage
        && typeof props.maxPage === 'number';

    return <div {...combineProps(props, 'data-cont')}>
        <header>
            <span className='header-text'>{props.heading}</span>
            {paginated && <PageButtons
                currentPage={props.currentPage!}
                onChangePage={props.onChangePage!}
                maxPage={props.maxPage!}
            />}
        </header>
        <div className='data-viewport'>
            {props.children}
        </div>
        {paginated && !props.hideBottomPageButtons && <PageButtons
            className='footer-page-buttons'
            currentPage={props.currentPage!}
            onChangePage={props.onChangePage!}
            maxPage={props.maxPage!}
        />}
    </div>;
}

interface PageButtonsProps extends ClassStyleProps {
    currentPage: number;
    maxPage: number;
    onChangePage: (newPage: number) => void;
}

function PageButtons(props: PageButtonsProps) {
    function renderLink(icon: string, target: number, disabled: boolean) {
        return <Button
            onClick={() => props.onChangePage(target)}
            disabled={disabled}>
            <i className={`fas fa-${icon}`} />
        </Button>;
    }

    const { currentPage, maxPage } = props;
    return (
        <div {...combineProps(props, 'page-buttons button-row row-content')}>
            {renderLink('angle-double-left', 0, currentPage <= 0)}
            {renderLink('angle-left', currentPage - 1, currentPage <= 0)}
            {renderLink('angle-right', currentPage + 1, currentPage >= maxPage)}
            {renderLink('angle-double-right', maxPage, currentPage >= maxPage)}
        </div>
    );
}
