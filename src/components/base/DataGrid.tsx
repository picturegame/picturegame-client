import * as React from 'react';

import { combineProps, ClassStyleProps } from '../../utils/CssUtils';

import { DataContainer, DataContainerProps } from './DataContainer';

import './DataGrid.scss';

export interface CellRenderer<T> {
    (item: T, index: number): React.ReactElement<any>;
}

export interface ColumnSpec<T> {
    id: string;
    name: string;
    cell: CellRenderer<T>;
    width?: number | string;
    style?: React.CSSProperties;
}

export interface DataGridProps<T> extends Partial<DataContainerProps>, ClassStyleProps {
    heading: string;
    columns: ColumnSpec<T>[];
    data: (T | undefined)[];
    rowRenderer: (item: T, cells: React.ReactElement<any>[]) => React.ReactElement<any>;
    fallbackRow?: () => React.ReactElement<any>;
    cellOverrides?: Record<string, CellRenderer<T>>;
    emptyState?: React.ReactElement<any>;
}

export const buildResolved = <S, T>(resolve: (item: S) => T | undefined, cell: CellRenderer<T>): CellRenderer<S> =>
    (item, index) => {
        const resolved = resolve(item);
        if (resolved) {
            return cell(resolved, index);
        }
        return <td><span className='loading-cell' /></td>;
    };

export function DataGrid<T>(props: DataGridProps<T>) {
    const rowTemplate = createRowTemplate(props.columns);

    function renderRow(item: T | undefined, index: number) {
        if (item === undefined) {
            return props.fallbackRow?.();
        }
        const cells = props.columns.map(c => {
            const cellRenderer = props.cellOverrides?.[c.id] ?? c.cell;
            const cell = cellRenderer(item, index);
            return React.cloneElement(cell, {
                ...combineProps(cell.props, '', c.style),
                key: c.id,
            });
        });

        const row = props.rowRenderer(item, cells);
        return React.cloneElement(row, {
            style: { gridTemplateColumns: rowTemplate },
        });
    }

    const empty = props.data.length === 0;
    return (
        <DataContainer
            heading={props.heading}
            currentPage={props.currentPage}
            onChangePage={props.onChangePage}
            maxPage={props.maxPage}
            hideBottomPageButtons={props.hideBottomPageButtons}
            className={props.className}
            style={props.style}>
            {empty && props.emptyState}
            {!empty && <table>
                <thead>
                    <tr style={{ gridTemplateColumns: rowTemplate }}>
                        {props.columns.map(c => <th key={c.id} style={c.style}>{c.name}</th>)}
                    </tr>
                </thead>
                <tbody>
                    {props.data.map(renderRow)}
                </tbody>
            </table>}
        </DataContainer>
    );
}

function createRowTemplate(columns: ColumnSpec<any>[]) {
    return columns.map(c => typeof c.width === 'number' ? `${c.width}px` : c.width ?? 'auto').join(' ');
}
