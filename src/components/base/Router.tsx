import { LocationDescriptorObject } from 'history';
import * as QueryString from 'query-string';
import * as React from 'react';

import { usePath, GlobalQueryParams, HistoryContext, QueryParam } from '../../hooks/History';

export interface RouterProps {
    children: React.ReactElement | Array<React.ReactElement>;
}

export function Router(props: RouterProps) {
    const path = usePath();

    let childNode: React.ReactNode;
    let rootNode: React.ReactNode;

    React.Children.forEach(props.children, c => {
        const childPath = (c.props as RouteProps).path;
        if (childPath === path) {
            childNode = c;
        } else if (childPath === '/') {
            rootNode = c;
        }
    });

    return <>{childNode ?? rootNode}</>;
}

export interface RouteProps {
    path: string;
    children: React.ReactChild | Array<React.ReactChild>;
}

export function Route(props: RouteProps) {
    return <>{props.children}</>;
}

export interface LinkProps {
    path: string;
    query?: { [key: string]: any };
    keepQuery?: boolean;
    children: (props: WrappedLinkProps) => React.ReactElement;
}

export interface WrappedLinkProps {
    href: string;
    onClick: (ev: React.MouseEvent<HTMLElement>) => void;
}

export function Link(props: LinkProps) {
    const history = React.useContext(HistoryContext);

    let queryString: string = '';
    if (props.keepQuery) {
        queryString = QueryString.stringify({
            ...QueryString.parse(history.location.search),
            ...props.query,
        });
    } else {
        const currentQuery = QueryString.parse(history.location.search);
        const nextQuery = { ...props.query };
        for (const key in currentQuery) {
            if (GlobalQueryParams.has(key as QueryParam)) {
                (nextQuery as any)[key] = currentQuery[key];
            }
        }
        queryString = QueryString.stringify(nextQuery);
    }

    const href = history.createHref({
        pathname: props.path,
        search: queryString,
    });

    function onClick(ev: React.MouseEvent<HTMLElement>) {
        if (ev.defaultPrevented) { return; }
        ev.preventDefault();

        const opts: LocationDescriptorObject = {
            pathname: props.path,
            search: queryString,
        };

        history.push(opts);
    }

    return props.children({
        href,
        onClick,
    });
}
