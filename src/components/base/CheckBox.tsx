import * as React from 'react';

import { Button } from './Button';

import { ClassStyleProps } from '../../utils/CssUtils';

export interface CheckBoxProps extends ClassStyleProps {
    checked: boolean;
    onToggle(): void;
}

export function CheckBox(props: CheckBoxProps) {
    return <Button className={props.className} style={props.style} onClick={props.onToggle}>
        <i className={'fas ' + (props.checked ? 'fa-check-square' : 'fa-square')} />
    </Button>;
}
