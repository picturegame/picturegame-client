import * as React from 'react';

import { combineProps, ClassStyleProps } from '../../utils/CssUtils';

import './ArrayPicker.scss';

export interface ArrayValueInputProps {
    value: string;
    onChange(value: string): void;
    onKeyDown(ev: React.KeyboardEvent<HTMLInputElement>): void;
    onSubmit(value: string): void;
    placeholder?: string;
    disabled?: boolean;
    clearOnSubmit?: boolean;
    className?: string;
}

export interface ArrayPickerProps extends ClassStyleProps {
    values: string[];
    setValues(values: string[]): void;
    inputComp(props: ArrayValueInputProps): JSX.Element;
    placeholder?: string;
    disabled?: boolean;
    formatChip?(value: string): string;
}

export function ArrayPicker(props: ArrayPickerProps) {
    const [inputValue, setInputValue] = React.useState('');

    function addItem(value: string) {
        props.setValues([...props.values, value]);
        setInputValue('');
    }

    function deleteItem(index: number) {
        props.setValues([...props.values.slice(0, index), ...props.values.slice(index + 1)]);
    }

    function onKeyDown(ev: React.KeyboardEvent<HTMLInputElement>) {
        if (props.disabled) {
            return;
        }

        if (ev.key === 'Tab' || ev.key === 'Enter') {
            ev.preventDefault();
            if (inputValue) {
                addItem(inputValue);
                setInputValue('');
            }
        }
    }

    return <div {...combineProps(props, 'array-picker-cont')} tabIndex={0}>
        {props.inputComp({
            onChange: setInputValue,
            onKeyDown,
            onSubmit: addItem,
            value: inputValue,
            placeholder: props.placeholder,
            disabled: props.disabled,
            clearOnSubmit: true,
            className: 'array-picker-input',
        })}
        <div className='array-picker-chips'>
            {props.values.map((v, i) => (
                <div className='array-picker-chip' key={i}>
                    <span>{props.formatChip ? props.formatChip(v) : v}</span>
                    {!props.disabled && <i className='fas fa-times delete-button' onClick={() => deleteItem(i)} />}
                </div>
            ))}
        </div>
    </div>;
}
