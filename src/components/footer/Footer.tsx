import * as React from 'react';

import { useState } from '../../hooks/Redux';

import * as UI from '../../model/ui';

import snoo from '../../../static/snoo.png';

import './Footer.scss';

function extractState(state: UI.State) {
    return {
        version: state.config.version,
        subreddit: state.config.subreddit,
        apiUrl: state.config.apiUrl,
        discordInvite: state.config.discordInvite,
    };
}

interface FooterLink {
    text: string;
    href: string;
}

interface FooterLinkGroup {
    heading: string;
    links: FooterLink[];
}

function getFooterLinkGroups({ apiUrl, discordInvite, subreddit }: ReturnType<typeof extractState>): FooterLinkGroup[] {
    return [
        {
            heading: 'Reddit',
            links: [
                { text: subreddit, href: `https://reddit.com/r/${subreddit}` },
                { text: 'PictureGame Bot News', href: 'https://reddit.com/r/Picturegame_Bot' },
            ],
        },
        {
            heading: 'Code',
            links: [
                { text: 'API Documentation', href: apiUrl },
                { text: 'API Source', href: 'https://gitlab.com/picturegame/picturegame-api' },
                { text: 'UI Source', href: 'https://gitlab.com/picturegame/picturegame-client' },
            ],
        },
        {
            heading: 'Community',
            links: [
                { text: 'Discord', href: `https://discord.gg/${discordInvite}` },
            ],
        },
    ];
}

export function Footer() {
    const stateProps = useState(extractState);
    const { subreddit, version } = stateProps;
    const linkGroups = getFooterLinkGroups(stateProps);

    return (
        <div className='app-footer'>
            <div className='app-info-col'>
                <img src={snoo} />
                <div className='app-title'>{subreddit}</div>
                <div className='app-desc'>UI version {version}</div>
                <div className='app-desc'>By <a href='https://reddit.com/u/provium' target='_blank'>/u/Provium</a></div>
            </div>
            <div className='footer-links'>
                {linkGroups.map(g => <LinkGroup key={g.heading} {...g} />)}
            </div>
        </div>
    );
}

function LinkGroup(group: FooterLinkGroup) {
    return (
        <div className='footer-link-group'>
            <div className='link-group-header'>{group.heading}</div>
            <ul>
                {group.links.map(l => <Link key={l.text} {...l} />)}
            </ul>
        </div>
    );
}

function Link(link: FooterLink) {
    return <li><a href={link.href} target='_blank'>{link.text}</a></li>;
}
