import * as API from 'picturegame-api-wrapper';
import * as React from 'react';
import * as Reselect from 'reselect';

import { useDateRange } from '../../hooks/Misc';
import { useState } from '../../hooks/Redux';

import * as UI from '../../model/ui';

import { calculateLinearDateTicks, calculateLinearTicks, getYRange, useChartZoom, ZoomableChartProps } from '../../utils/ChartUtils';
import { getColour } from '../../utils/CssUtils';
import { DateRange } from '../../utils/DateUtils';
import { formatDate } from '../../utils/Formatter';
import { createMemoizedArray } from '../../utils/Memoize';

import { ChartTooltip, ChartWrapper } from './Chart';

interface Props extends ZoomableChartProps {
    activePlayers: API.LeaderboardEntry[];
}

interface RoundCount {
    date: number;
    [username: string]: number;
}

function stateToProps(state: UI.State) {
    return {
        colours: state.userConfig.playerColours,
    };
}

const getRoundCounts: (activePlayers: API.LeaderboardEntry[], dateRange: DateRange) => RoundCount[] = Reselect.createSelector(
    createMemoizedArray((activePlayers: API.LeaderboardEntry[]) => activePlayers),
    (_: API.LeaderboardEntry[], dateRange: DateRange) => dateRange,
    function _getRoundCounts(activeUsers, dateRange): RoundCount[] {
        if (!activeUsers) { return []; }
        const counts: RoundCount[] = [];

        const currentWinCounts: { [name: string]: number } = {};

        function addDataPoint(date: number) {
            counts.push({ date, ...currentWinCounts });
        }

        const orderedWins: { username: string; date: number }[] = [];
        for (const user of activeUsers) {
            user.winTimeList!.forEach(date => {
                orderedWins.push({ username: user.username, date });
            });
        }
        orderedWins.sort((a, b) => a.date - b.date);

        for (const { username, date } of orderedWins) {
            currentWinCounts[username] = (currentWinCounts[username] || 0) + 1;
            addDataPoint(date);
        }

        const [, rangeEnd] = dateRange;
        const chartEnd = Math.floor((rangeEnd?.valueOf() ?? Date.now()) / 1000);
        addDataPoint(chartEnd);

        return counts;
    });

export function RoundsChart(props: Props) {
    const { colours } = useState(stateToProps);
    const dateRange = useDateRange();
    const roundCounts = getRoundCounts(props.activePlayers, dateRange);

    const zoomProps = useChartZoom({
        data: roundCounts,
        xKey: 'date',
        activeDomain: props.zoomToDomain,
        setActiveDomain: props.onZoom,
        dragToZoomActive: props.dragToZoomActive,
    });

    const [yMin, yMax] = getYRange(roundCounts, zoomProps.xMin, zoomProps.xMax, 'date',
        datum => props.activePlayers.map(p => datum[p.username]));
    const yTicks = calculateLinearTicks(yMin === 1 ? 0 : yMin, yMax);

    return <ChartWrapper
        header='Wins over time'
        loaded={true}
        isZoomed={props.zoomToDomain !== null}
        resetZoom={() => props.onZoom(null)}
        dragToZoomActive={props.dragToZoomActive}
        onToggleDragToZoom={props.onToggleDragToZoom}
        containerRef={zoomProps.containerRef}
        renderChart={({ LineChart, XAxis, Label, YAxis, Line, Tooltip, ReferenceArea }) => {
            return (<LineChart
                data={roundCounts}
                margin={{ top: 21, left: 20, bottom: 20, right: 20 }}
                onMouseDown={zoomProps.onMouseDown}
                onMouseMove={zoomProps.onMouseMove}
            >
                <XAxis
                    dataKey='date'
                    type='number'
                    domain={[zoomProps.xMin, zoomProps.xMax]}
                    ticks={calculateLinearDateTicks(zoomProps.xMin, zoomProps.xMax)}
                    tickFormatter={formatDate}
                    allowDataOverflow={true}
                >
                    <Label value='Date' position='bottom' offset={0} />
                </XAxis>
                <YAxis
                    ticks={yTicks}
                    allowDataOverflow={true}
                    domain={[yTicks[0], yTicks[yTicks.length - 1]]}
                >
                    <Label
                        value='Number of Wins'
                        angle={-90}
                        position='left'
                        offset={0}
                        style={{ textAnchor: 'middle' }} />
                </YAxis>
                {props.activePlayers.map((p, i) =>
                    <Line
                        key={p.username}
                        type='stepAfter'
                        dataKey={p.username}
                        stroke={colours[p.username] ?? getColour(i)}
                        dot={false}
                        isAnimationActive={false}
                    />)}
                <Tooltip labelFormatter={formatDate} content={<ChartTooltip />} />
                {zoomProps.dragStart && zoomProps.dragEnd ? (
                    <ReferenceArea x1={zoomProps.dragStart} x2={zoomProps.dragEnd} />
                ) : null}
            </LineChart>);
        }}
    />;
}
