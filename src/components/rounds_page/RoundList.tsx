import { Round } from 'picturegame-api-wrapper';
import * as React from 'react';

import { ApiContext, ApiError } from '../../api/PicturegameApi';
import { formatDateTimeShort, formatDuration } from '../../utils/Formatter';

import { RoundQuery } from '../../model/api';

import { DataContainer } from '../base/DataContainer';
import { Link } from '../base/Router';

import snoo from '../../../static/snoo_thumb.png';
import './RoundList.scss';

const PreloadNumPages = 5; // Preload 5 pages at a time to reduce number of requests
const RequiredRoundFields: ReadonlyArray<keyof Round> = [
    'roundNumber', 'id', 'postUrl', 'thumbnailUrl', 'title',
    'hostName', 'postTime', 'postDelay', 'abandoned', 'winnerName', 'winTime', 'solveTime',
];

interface CurrentLoadingState {
    filter?: string | null;
    sort?: string;
    offset: number;
}

export interface RoundListProps {
    filter: string;
    filterInvalid?: boolean;
    sort?: string;
    onRequestComplete?(success: boolean): void;
    header(numResults: number): string;
    page: number;
    onSetPage(newPage: number): void;
    roundsPerPage: number;
}

export function RoundList(props: RoundListProps) {
    const [rounds, setRounds] = React.useState<ReadonlyArray<Round>>([]);
    const [numResults, setNumResults] = React.useState(0);

    const loadingState = React.useRef<CurrentLoadingState | null>(null);

    const api = React.useContext(ApiContext);

    const firstRender = React.useRef(true);
    React.useEffect(() => {
        if (!firstRender.current) {
            props.onSetPage(0);
        }
        firstRender.current = false;
    }, [props.filter, props.sort]);

    const maxPage = Math.ceil(numResults / props.roundsPerPage) - 1;
    const apiRequestLimit = props.roundsPerPage * PreloadNumPages;
    const fetchOffset = Math.floor((props.page * props.roundsPerPage) / apiRequestLimit) * apiRequestLimit;

    React.useEffect(() => {
        if (maxPage > -1 && props.page > maxPage) {
            props.onSetPage(maxPage);
        }
    }, [props.page, maxPage]);

    React.useEffect(() => {
        const params: RoundQuery = {
            filter: props.filter,
            select: RequiredRoundFields,
            offset: fetchOffset,
            limit: apiRequestLimit,
            sort: props.sort,
        };
        loadingState.current = { filter: props.filter, sort: props.sort, offset: fetchOffset };

        setRounds([]);

        api.getRoundListResponse(params)
            .then(res => {
                if (loadingState.current?.filter !== props.filter || loadingState.current?.offset !== fetchOffset) {
                    // Started fetching a different page while this request was in-flight
                    return;
                }
                loadingState.current = null;

                setNumResults(res.totalNumResults);
                setRounds(res.results);
                props.onRequestComplete && props.onRequestComplete(true);
            })
            .catch(e => {
                loadingState.current = null;
                if (e instanceof ApiError && e.status === 400) {
                    props.onRequestComplete && props.onRequestComplete(false);
                }
            });

    }, [fetchOffset, props.filter, props.sort]);

    const startIndex = (props.page % PreloadNumPages) * props.roundsPerPage;

    return (
        <div className='round-browser-list-cont'>
            <DataContainer
                heading={props.header(numResults)}
                currentPage={props.page}
                maxPage={maxPage}
                onChangePage={props.onSetPage}
            >
                {rounds.slice(startIndex, startIndex + props.roundsPerPage)
                    .map(round => <RoundDisplay key={round.roundNumber} round={round} />)}
                {rounds.length === 0 && <EmptyState filterInvalid={!!props.filterInvalid} loading={loadingState.current !== null} />}
            </DataContainer>
        </div>
    );
}

interface RoundDisplayProps {
    round: Round;
}

function RoundDisplay({ round }: RoundDisplayProps) {
    function renderPlayerLink(username?: string) {
        if (!username) {
            return '[deleted]';
        }
        return <Link path='/dashboard' query={{ player: username }}>
            {props => <a {...props}>{username}</a>}
        </Link>;
    }

    function getStatus() {
        if (round.winTime) {
            return <div className='round-info-row'>
                Solved in {formatDuration(round.solveTime)} by {renderPlayerLink(round.winnerName)}
            </div>;
        }

        if (round.abandoned) {
            return <div className='round-info-row'><strong>Abandoned</strong></div>;
        }

        return <div className='round-info-row'><strong>Unsolved</strong></div>;
    }

    function getThumbnailUrl() {
        if (round.thumbnailUrl?.startsWith('http')) {
            if (round.thumbnailUrl.startsWith('https')) {
                return round.thumbnailUrl;
            }
            return round.thumbnailUrl.replace('http', 'https');
        }
        return snoo;
    }

    const expectedPrefix = `[round ${round.roundNumber}]`;
    const incorrectTitle = !round.title?.toLowerCase().startsWith(expectedPrefix);

    return <div className='round-list-item-cont'>
        <a href={round.postUrl} target='_blank' className='thumbnail-cont'>
            <img src={getThumbnailUrl()} />
        </a>
        <div className='round-info-cont'>
            <header><a href={`https://redd.it/${round.id}`} target='_blank'>{round.title}</a></header>
            <div className='round-info-body'>
                <div className='round-info-row'>
                    {incorrectTitle && <span>Round {round.roundNumber}</span>}
                    <span>Host: {renderPlayerLink(round.hostName)}</span>
                </div>
                <div className='round-info-row'>
                    Posted: {formatDateTimeShort(round.postTime)} {round.postDelay && `(${formatDuration(round.postDelay)})`}
                </div>
                {getStatus()}
            </div>
        </div>
    </div>;
}

interface EmptyStateProps {
    filterInvalid: boolean;
    loading: boolean;
}

function EmptyState(props: EmptyStateProps) {
    if (props.filterInvalid) {
        return <div className='empty-state'>
            <i className='fas fa-exclamation-circle highlight-error icon' />
            <span>The filter you entered is invalid.</span>
        </div>;
    }
    if (props.loading) {
        return <div className='empty-state'>
            <div className='loader icon' />
        </div>;
    }
    return <div className='empty-state'>
        <i className='fas fa-info-circle icon' />
        <span>No results. Update the filter and try again.</span>
    </div>;
}
