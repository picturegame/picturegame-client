import * as React from 'react';

import { useQueryString, QueryParam } from '../../hooks/History';

import { combineProps, ClassStyleProps } from '../../utils/CssUtils';
import { getDateRange, DateRange, PreCannedDateRange } from '../../utils/DateUtils';
import { formatDate, formatMonthYear } from '../../utils/Formatter';

import './LeaderboardFilter.scss';

export interface LeaderboardFilterProps extends ClassStyleProps {
}

export function LeaderboardFilter(props: LeaderboardFilterProps) {
    const [showDateFilters, setShowDateFilters] = React.useState(false);

    const [selectedFilter, onChangeFilter] = useQueryString(QueryParam.DateFilter);
    const [customStartDate, setCustomStartDate] = useQueryString(QueryParam.CustomStartDate);
    const [customEndDate, setCustomEndDate] = useQueryString(QueryParam.CustomEndDate);
    const dateRange = getDateRange({ rangeType: selectedFilter, customStart: customStartDate, customEnd: customEndDate });

    function renderFilterOption(filter: PreCannedDateRange | undefined, text: string) {
        return <DateRangeOption
            value={filter}
            text={text}
            onClick={() => onChangeFilter(filter)}
            active={selectedFilter === filter}
        />;
    }

    return (
        <div {...combineProps(props, 'leaderboard-filter-cont')}>
            <div className='leaderboard-filter-chips'>
                <div className='leaderboard-filter-chip' onClick={() => setShowDateFilters(v => !v)}>
                    <div className='leaderboard-filter-chip-text'>
                        <i className='fas fa-calendar' />
                        <span className='leaderboard-chip-text'>{getDateRangeText(dateRange, selectedFilter)}</span>
                    </div>
                    <i className={'fas fa-chevron-' + (showDateFilters ? 'up' : 'down')} />
                </div>
            </div>
            {showDateFilters && <div className='date-filter-options'>
                <div className='date-filters-group'>
                    {renderFilterOption(undefined, 'All Time')}
                </div>
                <div className='date-filters-group'>
                    {renderFilterOption(PreCannedDateRange.Abs_Today, 'Today')}
                    {renderFilterOption(PreCannedDateRange.Abs_ThisWeek, 'This Week')}
                    {renderFilterOption(PreCannedDateRange.Abs_ThisMonth, 'This Month')}
                    {renderFilterOption(PreCannedDateRange.Abs_ThisYear, 'This Year')}
                </div>
                <div className='date-filters-group'>
                    {renderFilterOption(PreCannedDateRange.Abs_Yesterday, 'Yesterday')}
                    {renderFilterOption(PreCannedDateRange.Abs_LastWeek, 'Last Week')}
                    {renderFilterOption(PreCannedDateRange.Abs_LastMonth, 'Last Month')}
                    {renderFilterOption(PreCannedDateRange.Abs_LastYear, 'Last Year')}
                </div>
                <div className='date-filters-group'>
                    {renderFilterOption(PreCannedDateRange.Rel_Day, 'Past 24 Hours')}
                    {renderFilterOption(PreCannedDateRange.Rel_Week, 'Past 7 Days')}
                    {renderFilterOption(PreCannedDateRange.Rel_30Day, 'Past 30 Days')}
                    {renderFilterOption(PreCannedDateRange.Rel_60Day, 'Past 60 Days')}
                    {renderFilterOption(PreCannedDateRange.Rel_90Day, 'Past 90 Days')}
                </div>
                <div className='date-filters-group'>
                    {renderFilterOption(PreCannedDateRange.Custom, 'Custom')}
                    {selectedFilter === PreCannedDateRange.Custom && <div className='custom-range-cont'>
                        <input type='date'
                            value={customStartDate ?? ''}
                            onChange={ev => setCustomStartDate(ev.target.value)}
                        />
                        <span> - </span>
                        <input type='date'
                            value={customEndDate ?? ''}
                            onChange={ev => setCustomEndDate(ev.target.value)}
                        />
                    </div>}
                </div>
            </div>}
        </div>
    );
}

function getDateRangeText([rangeStart, rangeEnd]: DateRange, selectedFilter?: string | null) {
    if (selectedFilter === PreCannedDateRange.Custom) {
        if (!rangeStart) {
            if (!rangeEnd) {
                return 'All Time';
            }

            return `Before ${formatDate(rangeEnd)}`;
        }

        if (!rangeEnd) {
            return `After ${formatDate(rangeStart)}`;
        }

        return `${formatDate(rangeStart)} - ${formatDate(rangeEnd)}`;
    }

    switch (selectedFilter) {
        case PreCannedDateRange.Abs_Today:
        case PreCannedDateRange.Abs_Yesterday:
            return `${formatDate(rangeStart!)}`;
        case PreCannedDateRange.Abs_ThisWeek:
        case PreCannedDateRange.Abs_LastWeek:
            return `Week of ${formatDate(rangeStart!)}`;
        case PreCannedDateRange.Abs_ThisMonth:
        case PreCannedDateRange.Abs_LastMonth:
            return `${formatMonthYear(rangeStart!)}`;
        case PreCannedDateRange.Abs_ThisYear:
        case PreCannedDateRange.Abs_LastYear:
            return `Year ${rangeStart!.getUTCFullYear()}`;

        case PreCannedDateRange.Rel_Day: return 'Past 24 Hours';
        case PreCannedDateRange.Rel_Week: return 'Past 7 Days';
        case PreCannedDateRange.Rel_30Day: return 'Past 30 Days';
        case PreCannedDateRange.Rel_60Day: return 'Past 60 Days';
        case PreCannedDateRange.Rel_90Day: return 'Past 90 Days';

        default: return 'All Time';
    }
}

interface DateRangeOptionProps {
    value?: PreCannedDateRange;
    text: string;
    onClick(): void;
    active: boolean;
}

function DateRangeOption(props: DateRangeOptionProps) {
    return <div className={'date-range-option' + (props.active ? ' active' : '')} onClick={props.onClick}>
        {props.text}
    </div>;
}
