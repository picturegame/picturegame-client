import { ArrayExpression, IToken, TokenType } from 'picturegame-api-wrapper';

import { formatDate, formatDateTimeShort, formatDuration } from '../../utils/Formatter';
import { parseDateTime } from '../../utils/Parser';

export function FormatRaw(value: IToken) {
    return value.input;
}

export function FormatDuration(value: IToken) {
    if (value.type !== TokenType.NumericValue) {
        return FormatRaw(value);
    }

    const durationSeconds = parseInt(value.input, 10);
    return formatDuration(durationSeconds)!;
}

export function FormatTruncated(value: IToken, maxChars: number) {
    if (value.input.length <= maxChars) {
        return value.input;
    }

    return value.input.slice(0, maxChars) + '...';
}

export function FormatDateTime(value: IToken) {
    if (value.type !== TokenType.DateTimeValue) {
        return FormatRaw(value);
    }

    const date = parseDateTime(value.input);
    if (!date) {
        return FormatRaw(value);
    }

    return (date.getUTCHours() > 0 || date.getUTCMinutes() > 0 || date.getUTCSeconds() > 0)
        ? formatDateTimeShort(date)!
        : formatDate(date)!;
}

export function FormatArray(input: ArrayExpression, valueFormatter: (input: IToken) => string, maxList: number = 3) {
    if (input.items.length > maxList) {
        return input.items.slice(0, maxList - 1).map(valueFormatter).join(', ') + `, and ${input.items.length - maxList + 1} more`;
    }
    return input.items.map(valueFormatter).join(', ');
}

export function FormatWeekday(value: IToken) {
    if (value.type !== TokenType.NumericValue) {
        return FormatRaw(value);
    }

    const index = parseInt(value.input, 10);
    return ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][index];
}
