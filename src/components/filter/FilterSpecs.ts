import { CompOperatorType, Round, TokenType } from 'picturegame-api-wrapper';

import { getTodayIsoString } from '../../utils/DateUtils';

import { PlayerSearch } from '../misc/PlayerSearch';

import { FilterSpec } from './FilterUtils';
import * as Formatters from './ValueFormatters';
import * as Inputs from './ValueInputs';

export const RoundFieldSpec: FilterSpec<Round> = {
    roundNumber: {
        valueType: TokenType.NumericValue,
        rValueChipFormat: Formatters.FormatRaw,
        rValueInputComponent: Inputs.SystemInput({ type: 'number' }),
    },
    title: {
        valueType: TokenType.StringValue,
        rValueChipFormat: v => Formatters.FormatTruncated(v, 20),
        rValueInputComponent: Inputs.SystemInput(),
        defaultOperator: CompOperatorType.Contains,
    },
    postUrl: {
        valueType: TokenType.StringValue,
        rValueChipFormat: v => Formatters.FormatTruncated(v, 20),
        rValueInputComponent: Inputs.SystemInput(),
    },
    thumbnailUrl: {
        valueType: TokenType.StringValue,
        rValueChipFormat: v => Formatters.FormatTruncated(v, 20),
        rValueInputComponent: Inputs.SystemInput(),
    },
    id: {
        valueType: TokenType.StringValue,
        rValueChipFormat: Formatters.FormatRaw,
        rValueInputComponent: Inputs.SystemInput(),
    },
    winningCommentId: {
        valueType: TokenType.StringValue,
        rValueChipFormat: Formatters.FormatRaw,
        rValueInputComponent: Inputs.SystemInput(),
    },
    hostName: {
        valueType: TokenType.StringValue,
        rValueChipFormat: Formatters.FormatRaw,
        rValueInputComponent: PlayerSearch,
    },
    postTime: {
        valueType: TokenType.DateTimeValue,
        rValueChipFormat: Formatters.FormatDateTime,
        rValueInputComponent: Inputs.SystemInput({ type: 'datetime-local' }),
        defaultValue: getTodayIsoString(),
        defaultOperator: CompOperatorType.GreaterOrEqual,
    },
    winnerName: {
        valueType: TokenType.StringValue,
        rValueChipFormat: Formatters.FormatRaw,
        rValueInputComponent: PlayerSearch,
    },
    winTime: {
        valueType: TokenType.DateTimeValue,
        rValueChipFormat: Formatters.FormatDateTime,
        rValueInputComponent: Inputs.SystemInput({ type: 'datetime-local' }),
        defaultValue: getTodayIsoString(),
        defaultOperator: CompOperatorType.GreaterOrEqual,
    },
    plusCorrectTime: {
        valueType: TokenType.DateTimeValue,
        rValueChipFormat: Formatters.FormatDateTime,
        rValueInputComponent: Inputs.SystemInput({ type: 'datetime-local' }),
        defaultValue: getTodayIsoString(),
        defaultOperator: CompOperatorType.GreaterOrEqual,
    },
    solveTime: {
        valueType: TokenType.NumericValue,
        rValueChipFormat: Formatters.FormatDuration,
        rValueInputComponent: Inputs.SystemInput({ type: 'number' }),
        defaultOperator: CompOperatorType.GreaterOrEqual,
    },
    plusCorrectDelay: {
        valueType: TokenType.NumericValue,
        rValueChipFormat: Formatters.FormatDuration,
        rValueInputComponent: Inputs.SystemInput({ type: 'number' }),
        defaultOperator: CompOperatorType.GreaterOrEqual,
    },
    postDelay: {
        valueType: TokenType.NumericValue,
        rValueChipFormat: Formatters.FormatDuration,
        rValueInputComponent: Inputs.SystemInput({ type: 'number' }),
        defaultOperator: CompOperatorType.GreaterOrEqual,
    },
    postDayOfWeek: {
        valueType: TokenType.NumericValue,
        rValueChipFormat: Formatters.FormatWeekday,
        rValueInputComponent: Inputs.WeekdayPicker,
    },
    postTimeOfDay: {
        valueType: TokenType.TimeValue,
        rValueChipFormat: Formatters.FormatRaw,
        rValueInputComponent: Inputs.TimePicker,
        defaultOperator: CompOperatorType.GreaterOrEqual,
    },
    winDayOfWeek: {
        valueType: TokenType.NumericValue,
        rValueChipFormat: Formatters.FormatWeekday,
        rValueInputComponent: Inputs.WeekdayPicker,
    },
    winTimeOfDay: {
        valueType: TokenType.TimeValue,
        rValueChipFormat: Formatters.FormatRaw,
        rValueInputComponent: Inputs.TimePicker,
        defaultOperator: CompOperatorType.GreaterOrEqual,
    },
    abandoned: {
        valueType: TokenType.BooleanValue,
        rValueChipFormat: Formatters.FormatRaw,
        rValueInputComponent: Inputs.BooleanPicker,
    },
    abandonedTime: {
        valueType: TokenType.DateTimeValue,
        rValueChipFormat: Formatters.FormatDateTime,
        rValueInputComponent: Inputs.SystemInput({ type: 'datetime-local' }),
        defaultValue: getTodayIsoString(),
        defaultOperator: CompOperatorType.GreaterOrEqual,
    },
};
