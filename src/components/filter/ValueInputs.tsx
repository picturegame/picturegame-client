import * as React from 'react';

import { useSyncedState } from '../../hooks/Misc';
import { combineProps, ClassStyleProps } from '../../utils/CssUtils';

import { Dropdown } from '../base/Dropdown';

export interface ValueInputProps extends ClassStyleProps {
    value: string;
    onChange(value: string): void;
    onSubmit?(value: string): void;
    onKeyDown?(ev: React.KeyboardEvent<HTMLInputElement>): void;
    placeholder?: string;
    disabled?: boolean;
    clearOnSubmit?: boolean;
}

export function SystemInput(inputProps?: React.InputHTMLAttributes<HTMLInputElement>) {
    return (props: ValueInputProps) => {
        return <input
            type={inputProps?.type ?? 'text'}
            {...combineProps(props, 'filter-selector')}
            placeholder={props.placeholder}
            disabled={props.disabled}
            value={props.value ?? ''}
            onChange={ev => props.onChange(ev.target.value)}
            onKeyDown={props.onKeyDown}
        />;
    };
}

const TimePattern = /^([0-9]{2}):([0-9]{2})(?::([0-9]{2}))?$/;
export function TimePicker(props: ValueInputProps) {
    function onChange(value: string) {
        const match = value.match(TimePattern);
        if (!match) {
            props.onChange('');
            return;
        }

        props.onChange(`${match[1]}:${match[2]}:${match[3] ?? '00'}`);
    }

    function convertInput(value: string) {
        const match = value?.match(TimePattern);
        if (!match) {
            return '00:00';
        }
        if (match[3] && match[3] !== '00') {
            return value;
        }
        return `${match[1]}:${match[2]}`;
    }

    return <input
        type='time'
        {...combineProps(props, 'filter-selector')}
        placeholder={props.placeholder}
        disabled={props.disabled}
        value={convertInput(props.value)}
        onChange={ev => onChange(ev.target.value)}
    />;
}

export interface EnumPickerOption {
    value: string;
    translation?: string;
}

export function EnumPicker(options: EnumPickerOption[]) {
    const optionsByValue = new Map<string, EnumPickerOption>(options.map(o => [o.value, o]));

    return (props: ValueInputProps) => {
        const [inputValue, setInputValue] = useSyncedState(
            () => optionsByValue.get(props.value)?.translation ?? props.value,
            [props.value]);
        const [invalid, setInvalid] = React.useState(false);

        function renderSuggestion(suggestion: EnumPickerOption) {
            return <div>{suggestion.translation ?? suggestion.value}</div>;
        }

        function onSubmit(value: string) {
            if (!value) {
                return;
            }

            const valueLower = value.toLowerCase().trim();
            if (props.clearOnSubmit) {
                setInputValue('');
            }

            for (const option of options) {
                if (option.value.toLowerCase() === valueLower || (option.translation && option.translation.toLowerCase() === valueLower)) {
                    props.onChange(option.value);
                    props.onSubmit && props.onSubmit(option.value);
                    setInvalid(false);
                    return;
                }
            }

            setInvalid(true);
        }

        return <Dropdown
            value={inputValue}
            onChange={setInputValue}
            placeholder={props.placeholder}
            suggestions={options}
            getSuggestionValue={o => o.value}
            renderSuggestion={renderSuggestion}
            onSubmit={onSubmit}
            disabled={props.disabled}
            {...combineProps(props, {
                'filter-selector': true,
                'invalid': invalid,
            })}
            showChevron
        />;
    };
}

export const BooleanPicker = EnumPicker([{ value: 'true' }, { value: 'false' }]);
export const WeekdayPicker = EnumPicker([
    { value: '0', translation: 'Sunday' },
    { value: '1', translation: 'Monday' },
    { value: '2', translation: 'Tuesday' },
    { value: '3', translation: 'Wednesday' },
    { value: '4', translation: 'Thursday' },
    { value: '5', translation: 'Friday' },
    { value: '6', translation: 'Saturday' },
]);
