import * as React from 'react';

import { DeletePopup } from '../../actions/ViewActions';
import { useDispatch, useState } from '../../hooks/Redux';
import { classNameFromObj } from '../../utils/CssUtils';

import * as UI from '../../model/ui';

import './PopupMessages.scss';

interface PopupProps {
    popup: UI.PopupMessage;
}
function getDeletePopup(dispatch: UI.DispatchAction, id: string) {
    return () => dispatch(DeletePopup({ id }));
}

function Popup({ popup }: PopupProps) {
    const onDelete = useDispatch(getDeletePopup, popup.id);

    React.useEffect(() => {
        if (!popup.timeout) { return; }
        const timer = setTimeout(onDelete, popup.timeout);
        return () => clearTimeout(timer);
    }, [popup.timeout]);

    const className = classNameFromObj({
        'popup-message': true,
        'popup-info': popup.type === 'info',
        'popup-warn': popup.type === 'warn',
        'popup-error': popup.type === 'error',
    });

    return (
        <div className={className}>
            <div className='popup-message-contents'>{popup.message}</div>
            {popup.dismissable && <i className='fas fa-times' onClick={onDelete} />}
        </div>
    );
}

function extractState(state: UI.State) {
    return {
        popups: state.view.popups,
    };
}

export function PopupContainer() {
    const { popups } = useState(extractState);

    if (!popups.length) { return null; }

    return (
        <div className='popups-container'>
            {popups.map(p => <Popup key={p.id} popup={p} />)}
        </div>
    );
}
