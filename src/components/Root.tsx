import * as React from 'react';

import * as UI from '../model/ui';

import { ClearPlayerData } from '../actions/DataActions';
import { ApiContext } from '../api/PicturegameApi';
import { useDateRange } from '../hooks/Misc';
import { useDispatch, useState } from '../hooks/Redux';
import { useToggle } from '../hooks/Toggle';

import { classNameFromObj } from '../utils/CssUtils';
import { convertToLeaderboardFilter } from '../utils/DateUtils';

import { LoadingState } from './base/LoadingState';
import { Route, Router } from './base/Router';
import { Footer } from './footer/Footer';
import { PopupContainer } from './misc/PopupMessages';
import { Sidebar } from './sidebar/Sidebar';
import { Toolbar } from './toolbar/Toolbar';

const ComparisonPage = React.lazy(() => import(/* webpackChunkName: "Compare" */'./compare/ComparisonPage'));
const Dashboard = React.lazy(() => import(/* webpackChunkName: "Dashboard" */'./dashboard/Dashboard'));
const Leaderboard = React.lazy(() => import(/* webpackChunkName: "Leaderboard" */'./leaderboard/Leaderboard'));
const RoundsPage = React.lazy(() => import(/* webpackChunkName: "RoundsPage" */'./rounds_page/RoundsPage'));
// const SandboxPage = React.lazy(() => import(/* webpackChunkName: "SandboxPage" */'./sandbox/Sandbox'));
const FactsPage = React.lazy(() => import(/* webpackChunkName: "FactsPage" */'./sandbox/FactsAndFigures'));

import './main.scss';

interface StateProps {
    theme: UI.ThemeSetting;
}

function stateToProps(state: UI.State): StateProps {
    return {
        theme: state.userConfig.theme ?? 'dark',
    };
}

function extractDispatch(dispatch: UI.DispatchAction) {
    return {
        clearPlayerData: () => dispatch(ClearPlayerData(undefined)),
    };
}

export function Root() {
    const { theme } = useState(stateToProps);
    const { clearPlayerData } = useDispatch(extractDispatch);

    const { value: sidebarOpen, setTrue: onOpenSidebar, setFalse: onCloseSidebar } = useToggle(false);

    React.useLayoutEffect(() => {
        document.getElementById('root')!.className = classNameFromObj({
            'theme-dark': theme === 'dark',
            'theme-light': theme === 'light',
        });
    }, [theme]);

    const dateRange = useDateRange();
    const api = React.useContext(ApiContext);
    React.useEffect(() => {
        clearPlayerData();
        const leaderboardFilter = convertToLeaderboardFilter(dateRange);
        api.getLeaderboard(leaderboardFilter);
    }, [dateRange, api, clearPlayerData]);

    return (<>
        <Toolbar
            onOpenSidebar={onOpenSidebar}
        />
        <div className='app-body-main'>
            <React.Suspense fallback={<LoadingState />}>
                <Router>
                    <Route path='/'>
                        <Leaderboard />
                    </Route>
                    <Route path='/compare'>
                        <ComparisonPage />
                    </Route>
                    <Route path='/dashboard'>
                        <Dashboard />
                    </Route>
                    <Route path='/rounds'>
                        <RoundsPage />
                    </Route>
                    {/*<Route path='/sandbox'>
                        <SandboxPage />
                    </Route>*/}
                    <Route path='/facts'>
                        <FactsPage />
                    </Route>
                </Router>
            </React.Suspense>
        </div>
        <Footer />
        <PopupContainer />
        <Sidebar open={sidebarOpen} onClose={onCloseSidebar} />
    </>);
}
