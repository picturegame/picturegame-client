import * as API from 'picturegame-api-wrapper';
import * as React from 'react';

import { useQueryString, QueryParam } from '../../hooks/History';
import { useDateRange, useDocumentTitle } from '../../hooks/Misc';
import { useState } from '../../hooks/Redux';

import * as UI from '../../model/ui';

import { useZoomState } from '../../utils/ChartUtils';
import { convertToLeaderboardFilter } from '../../utils/DateUtils';

import { LoadingState } from '../base/LoadingState';
import { RankChart } from '../charts/RankChart';
import { RoundsChart } from '../charts/RoundsChart';
import { WeeklyWinsChart } from '../charts/WeeklyWinsChart';
import { LeaderboardFilter } from '../leaderboard/LeaderboardFilter';
import { RoundList } from '../rounds_page/RoundList';

import { PlayerOverview } from './PlayerOverview';

import './Dashboard.scss';

function getActiveUser(state: UI.State, username: string): API.LeaderboardEntry | undefined {
    return state.data.leaderboard.get(username?.toLowerCase());
}

function stateToProps(state: UI.State, viewUser: string) {
    return {
        activeUser: getActiveUser(state, viewUser),
        leaderboardLoaded: state.leaderboard.dataLoaded,
    };
}

export default function Dashboard() {
    const [viewUser] = useQueryString(QueryParam.Player);
    const { activeUser, leaderboardLoaded } = useState(stateToProps, viewUser);

    useDocumentTitle(() => {
        return activeUser ? `${activeUser.username}'s Dashboard` : 'Dashboard';
    }, [activeUser]);

    if (!viewUser) {
        return null;
    }

    if (activeUser) {
        return <DashboardBody activeUser={activeUser} />;
    }

    return !leaderboardLoaded ? <LoadingState /> : renderPlayerNotFound(viewUser);
}

function renderPlayerNotFound(username: string) {
    return (<div className='user-error-state'>
        No such player "{username}"
    </div>);
}

function getChartVisibility(state: UI.State) {
    return state.userConfig.chartVisibility;
}

function DashboardBody(props: { activeUser: API.LeaderboardEntry }) {
    const zoomProps = useZoomState();
    const chartVisibility = useState(getChartVisibility);

    const [roundsPage, setRoundsPage] = React.useState(0);

    const dateRange = useDateRange();
    const dateFilterString = convertToLeaderboardFilter(dateRange);
    const filterString = `winnerName eq "${props.activeUser.username}"` + (dateFilterString ? ` and ${dateFilterString}` : '');

    return (<div className='page-viewport'>
        <div className='page-body'>
            <LeaderboardFilter />
            <PlayerOverview player={props.activeUser} />
            <div className='dashboard-charts'>
                {chartVisibility.rounds && <RoundsChart activePlayers={[props.activeUser]} {...zoomProps} />}
                {chartVisibility.ranks && <RankChart activePlayers={[props.activeUser]} {...zoomProps} />}
                {chartVisibility.weekly_wins && <WeeklyWinsChart activePlayers={[props.activeUser]} {...zoomProps} />}
            </div>
            <RoundList
                filter={filterString}
                sort='winTime asc'
                header={() => 'Rounds Won'}
                roundsPerPage={20}
                page={roundsPage}
                onSetPage={setRoundsPage}
            />
        </div>
    </div>);
}
