import * as React from 'react';

import {
    formatValue, inferDataType,
    ChartConfig, ChartData, CustomChartProps, CustomChartType,
} from './CustomChartHelper';

import { ColumnSpec, DataGrid } from '../base/DataGrid';
import { Link } from '../base/Router';

import { FieldValueType } from '../../utils/ChartUtils';

export interface ColumnConfig {
    key: string;
    width?: number;
}

export interface TableConfig extends ChartConfig {
    chartType: CustomChartType.Table;
    series: ColumnConfig[];
}

export function isTable(config: ChartConfig): config is TableConfig {
    return config.chartType === CustomChartType.Table;
}

export function initTable(): TableConfig {
    return {
        chartType: CustomChartType.Table,
        header: 'Table Title',
        primaryKey: '',
        series: [],
    };
}

const MaxRows = 10;

interface TableProps<TElem, TGroup> {
    outerProps: CustomChartProps<TElem, TGroup>;
    config: TableConfig;
}
export function TableRenderer<TElem, TGroup>(
    { outerProps: props, config }: TableProps<TElem, TGroup>) {

    const [page, setPage] = React.useState(0);
    const maxPage = Math.max(0, Math.ceil((props.data?.length ?? 0) / MaxRows) - 1);

    const dataPage = React.useMemo(() => props.data?.slice(page * MaxRows, (page + 1) * MaxRows) ?? [], [props.data, page]);

    const columns = config.series.map((series): ColumnSpec<ChartData> => {
        const valueType = props.groupingFieldTypes[series.key as keyof TElem] ?? props.metricFieldTypes[series.key as keyof TGroup];
        const dataType = series.key === 'index' ? 'number' : inferDataType(props.data, series.key, valueType);

        return {
            id: series.key,
            name: props.groupingFieldTranslations[series.key as keyof TElem] ?? props.metricFieldTranslations[series.key as keyof TGroup],
            cell: (item, index) => (<td className={dataType === 'number' ? 'col-numeric' : undefined}>
                {valueType === FieldValueType.Username
                    ? <Link path='/dashboard' query={{ player: item[series.key] }}>
                        {linkProps => <a {...linkProps}>{item[series.key]}</a>}
                    </Link>
                    : formatValue(series.key === 'index' ? (page * MaxRows + index + 1) : item[series.key], dataType)}
            </td>),
            width: series.width,
        };
    });

    return <div className='custom-table-cont'>
        <DataGrid
            heading={config.header}
            columns={columns}
            data={dataPage}
            currentPage={page}
            maxPage={maxPage}
            onChangePage={setPage}
            hideBottomPageButtons
            rowRenderer={(item, content) => <tr key={item[config.primaryKey]}>{content}</tr>}
        />
    </div>;
}
