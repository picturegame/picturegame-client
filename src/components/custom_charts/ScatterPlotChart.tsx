import * as _ from 'lodash-es';
import * as React from 'react';
import { TooltipFormatter } from 'recharts';

import {
    formatValue, getLegendProps, inferDataType,
    ChartConfig, ChartValue, CustomChartProps, CustomChartType,
} from './CustomChartHelper';

import { renderXLabel, renderYLabel, ChartTooltip } from '../charts/Chart';

import { calculateLinearTicks } from '../../utils/ChartUtils';
import { getColour } from '../../utils/CssUtils';

export interface ScatterPlotConfig extends ChartConfig {
    chartType: CustomChartType.Scatter;
    xKey: string;
    yKey: string;
    colour?: string;
}
export function isScatterPlot(config: ChartConfig): config is ScatterPlotConfig {
    return config.chartType === CustomChartType.Scatter;
}

export function initScatterPlot(): ScatterPlotConfig {
    return {
        chartType: CustomChartType.Scatter,
        header: 'Chart Title',
        primaryKey: '',
        xKey: '',
        yKey: '',
        colour: getColour(0),
    };
}

interface ScatterPlotProps<TElem, TGroup> {
    outerProps: CustomChartProps<TElem, TGroup>;
    config: ScatterPlotConfig;
    Recharts: typeof import('recharts');
}
export function ScatterPlotRenderer<TElem, TGroup>(
    { outerProps: props, config, Recharts, ...innerProps }: ScatterPlotProps<TElem, TGroup>) {

    const { ScatterChart, Scatter, XAxis, YAxis, Label, Legend, Tooltip } = Recharts;

    function getValueType(key: string) {
        return props.groupingFieldTypes[key as keyof TElem] ?? props.metricFieldTypes[key as keyof TGroup];
    }

    function getTranslation(key: string) {
        return props.groupingFieldTranslations[key as keyof TElem] ?? props.metricFieldTranslations[key as keyof TGroup];
    }

    const primaryValueType = getValueType(config.primaryKey);
    const primaryDataType = inferDataType(props.data, config.primaryKey, primaryValueType);

    const xAxisValueType = getValueType(config.xKey);
    const xAxisDataType = inferDataType(props.data, config.xKey, xAxisValueType);

    const yAxisValueType = getValueType(config.yKey);
    const yAxisDataType = inferDataType(props.data, config.yKey, yAxisValueType);

    const xMax = _.max((props.data ?? []).map(d => d[config.xKey] as number)) ?? 0;
    const xTicks = calculateLinearTicks(0, xMax, xAxisDataType);

    const yMax = _.max((props.data ?? []).map(d => d[config.yKey] as number)) ?? 0;
    const yTicks = calculateLinearTicks(0, yMax, yAxisDataType);

    const tooltipFormatter: TooltipFormatter = (value, _name, _payload, index) =>
        formatValue(value as ChartValue, index === 0 ? xAxisDataType : yAxisDataType);

    return <ScatterChart
        data={props.data}
        margin={{ top: 21, left: 20, bottom: 20, right: 20 }}
        {...innerProps}
    >
        <XAxis
            dataKey={config.xKey}
            name={getTranslation(config.xKey)}
            type='number'
            ticks={xTicks}
            tickFormatter={val => formatValue(val, xAxisDataType)}
            domain={[xTicks[0], xTicks[xTicks.length - 1]]}
        >
            {renderXLabel(Label, getTranslation(config.xKey))}
        </XAxis>
        <YAxis
            dataKey={config.yKey}
            name={getTranslation(config.yKey)}
            type='number'
            ticks={yTicks}
            tickFormatter={val => formatValue(val, yAxisDataType)}
            domain={[yTicks[0], yTicks[yTicks.length - 1]]}
        >
            {renderYLabel(Label, getTranslation(config.yKey))}
        </YAxis>
        {config.legendType && <Legend
            {...getLegendProps(config.legendType)}
            formatter={() => getTranslation(config.primaryKey)}
        />}
        <Tooltip
            content={<ChartTooltip getLabel={payload => payload?.[0]?.payload?.[config.primaryKey]} />}
            labelFormatter={label => formatValue(label, primaryDataType)}
            formatter={tooltipFormatter}
        />
        <Scatter fill={config.colour ?? getColour(0)} />
    </ScatterChart>;
}
