import * as React from 'react';
import { TooltipFormatter } from 'recharts';

import {
    formatValue, getLegendProps, inferDataType,
    ChartConfig, ChartValue, CustomChartProps, CustomChartType, SeriesConfig,
} from './CustomChartHelper';

import { renderXLabel, renderYLabel, ChartTooltip } from '../charts/Chart';

import { calculateLinearDateTicks, calculateLinearTicks, getYRange, ChartZoomHooks, ValueTypeNames } from '../../utils/ChartUtils';
import { getColour } from '../../utils/CssUtils';
import * as Formatter from '../../utils/Formatter';

export interface SeriesConfigTimeSeries extends SeriesConfig {
    display: 'line' | 'bar';
}

export interface TimeSeriesConfig extends ChartConfig {
    chartType: CustomChartType.TimeSeries;
    series: SeriesConfigTimeSeries[];
}
export function isTimeSeries(config: ChartConfig): config is TimeSeriesConfig {
    return config.chartType === CustomChartType.TimeSeries;
}

export function initTimeSeries(): TimeSeriesConfig {
    return {
        chartType: CustomChartType.TimeSeries,
        header: 'Chart Title',
        primaryKey: '',
        series: [],
    };
}

interface TimeSeriesProps<TElem, TGroup> {
    outerProps: CustomChartProps<TElem, TGroup>;
    config: TimeSeriesConfig;
    zoomProps: ChartZoomHooks;
    Recharts: typeof import('recharts');
}
export function TimeSeriesRenderer<TElem, TGroup>(
    { outerProps: props, config, zoomProps, Recharts, ...innerProps }: TimeSeriesProps<TElem, TGroup>) {

    const { ComposedChart, Line, Bar, XAxis, YAxis, Label, Legend, Tooltip, ReferenceArea } = Recharts;

    const [legendHovered, setLegendHovered] = React.useState<ChartValue | null>(null);
    function onMouseEnter(payload: any) {
        setLegendHovered(payload.dataKey ?? payload.value);
    }
    function onMouseLeave() {
        setLegendHovered(null);
    }

    const xAxisValueType = props.groupingFieldTypes[config.primaryKey as keyof TElem];
    const xAxisDataType = inferDataType(props.data, config.primaryKey, xAxisValueType);

    const seriesValueTypes = config.series.map(s => props.metricFieldTypes[s.key as keyof TGroup]);
    const seriesDataTypes = config.series.map((s, i) => inferDataType(props.data, s.key, seriesValueTypes[i]));

    const yAxisFirstSeriesIdx = config.series.findIndex(series => (series.axis ?? 1) === 1);
    const yAxisValueType = seriesValueTypes[yAxisFirstSeriesIdx];
    const yAxisDataType = seriesDataTypes[yAxisFirstSeriesIdx];

    const yAxis2FirstSeriesIdx = config.series.findIndex(series => series.axis === 2);
    const needYAxis2 = yAxis2FirstSeriesIdx > -1;
    const yAxis2ValueType = seriesValueTypes[yAxis2FirstSeriesIdx];
    const yAxis2DataType = seriesDataTypes[yAxis2FirstSeriesIdx];

    const tooltipFormatter: TooltipFormatter = (value, _name, _payload, index) => {
        return formatValue(value as ChartValue, seriesDataTypes[index]);
    };

    const ySeries = config.series.filter(s => s.axis !== 2);
    const yRange = getYRange(props.data ?? [], zoomProps.xMin, zoomProps.xMax, config.primaryKey,
        datum => ySeries.map(s => datum[s.key] as number));
    if (yRange[0] <= 1 || ySeries.some(s => s.display === 'bar')) {
        yRange[0] = 0;
    }
    const yTicks = calculateLinearTicks(yRange[0], yRange[1], yAxisDataType);

    const y2Series = config.series.filter(s => s.axis === 2);
    const y2Range = getYRange(props.data ?? [], zoomProps.xMin, zoomProps.xMax, config.primaryKey,
        datum => y2Series.map(s => datum[s.key] as number));
    if (y2Range[0] <= 1 || y2Series.some(s => s.display === 'bar')) {
        y2Range[0] = 0;
    }
    const y2Ticks = calculateLinearTicks(y2Range[0], y2Range[1], yAxis2DataType);

    return <ComposedChart
        data={props.data}
        margin={{ top: 21, left: 20, bottom: 20, right: 20 }}
        {...innerProps}
        onMouseDown={zoomProps.onMouseDown}
        onMouseMove={zoomProps.onMouseMove}
    >
        <XAxis
            dataKey={config.primaryKey}
            type='number'
            domain={[zoomProps.xMin, zoomProps.xMax]}
            ticks={calculateLinearDateTicks(zoomProps.xMin, zoomProps.xMax)}
            tickFormatter={Formatter.formatDate}
            allowDataOverflow={true}
        >
            {renderXLabel(Label, props.groupingFieldTranslations[config.primaryKey as keyof TElem])}
        </XAxis>
        <YAxis
            yAxisId={1}
            allowDataOverflow={true}
            ticks={yTicks}
            tickFormatter={val => formatValue(val, yAxisDataType)}
            domain={[yRange[0], yTicks[yTicks.length - 1]]}
        >
            {renderYLabel(Label, ValueTypeNames[yAxisValueType])}
        </YAxis>
        {needYAxis2 && <YAxis
            yAxisId={2}
            orientation='right'
            allowDataOverflow={true}
            ticks={y2Ticks}
            tickFormatter={val => formatValue(val, yAxis2DataType)}
            domain={[y2Range[0], y2Ticks[y2Ticks.length - 1]]}
        >
            {renderYLabel(Label, ValueTypeNames[yAxis2ValueType], 'right')}
        </YAxis>}
        {config.legendType && <Legend
            {...getLegendProps(config.legendType)}
            onMouseEnter={onMouseEnter}
            onMouseLeave={onMouseLeave}
        />}
        <Tooltip
            labelFormatter={value => formatValue(value, xAxisDataType)}
            content={<ChartTooltip />}
            formatter={tooltipFormatter}
        />
        {zoomProps.dragStart && zoomProps.dragEnd ? (
            <ReferenceArea x1={zoomProps.dragStart} x2={zoomProps.dragEnd} yAxisId={1} />
        ) : null}
        {config.series.filter(s => s.display === 'bar').map((series, i) =>
            <Bar
                key={series.key}
                dataKey={series.key}
                name={props.metricFieldTranslations[series.key as keyof TGroup]}
                fill={series.colour ?? getColour(i)}
                isAnimationActive={false}
                opacity={(legendHovered && legendHovered !== series.key) ? 0.5 : 1}
                yAxisId={series.axis ?? 1}
            />)}
        {config.series.filter(s => s.display === 'line').map((series, i) =>
            <Line
                key={series.key}
                dataKey={series.key}
                name={props.metricFieldTranslations[series.key as keyof TGroup]}
                stroke={series.colour ?? getColour(i)}
                isAnimationActive={false}
                dot={false}
                strokeWidth={2}
                opacity={(legendHovered && legendHovered !== series.key) ? 0.5 : 1}
                yAxisId={series.axis ?? 1}
            />)}
    </ComposedChart>;
}
