import * as React from 'react';

import { ChartConfig, CustomChartProps } from './CustomChartHelper';

import { isBarChart, BarChartRenderer } from './BarChart';
import { isPieChart, PieChartRenderer } from './PieChart';
import { isScatterPlot, ScatterPlotRenderer } from './ScatterPlotChart';
import { isTimeSeries, TimeSeriesRenderer } from './TimeSeriesChart';

import { ChartRenderer, ChartWrapper } from '../charts/Chart';

import { useChartZoom, ChartZoomHooks } from '../../utils/ChartUtils';
import { combineProps } from '../../utils/CssUtils';
import { isTable, TableRenderer } from './Table';

export function CustomChart<TElem, TGroup>(props: CustomChartProps<TElem, TGroup>) {
    const classNameObj = {
        [getWidthClass(props.chartConfig.width)]: true,
        'with-vertical-legend': props.chartConfig.legendType === 'right',
        'with-horizontal-legend': props.chartConfig.legendType === 'bottom',
    };

    const zoomProps = useChartZoom({
        data: props.data,
        activeDomain: props.zoomToDomain,
        dragToZoomActive: props.dragToZoomActive,
        setActiveDomain: props.onZoom,
        xKey: props.chartConfig.primaryKey,
    });

    if (isTable(props.chartConfig)) {
        return <TableRenderer outerProps={props} config={props.chartConfig} />;
    }

    return <ChartWrapper
        {...combineProps(props, classNameObj)}
        header={props.chartConfig.header}
        loaded={props.loaded}
        isZoomed={props.zoomToDomain !== null}
        resetZoom={() => props.onZoom(null)}
        dragToZoomActive={props.dragToZoomActive}
        onToggleDragToZoom={props.onToggleDragToZoom}
        containerRef={zoomProps.containerRef}
        zoomable={isZoomable(props.chartConfig)}
        renderChart={createChartRenderer(props, zoomProps)}
    />;
}

function isZoomable(chart: ChartConfig) {
    return isTimeSeries(chart);
}

function getWidthClass(width?: number) {
    switch (width) {
        case 3: return 'triple-width';
        case 2: return 'double-width';
        case 1:
        default:
            return 'single-width';
    }
}

function createChartRenderer<TElem, TGroup>(props: CustomChartProps<TElem, TGroup>, zoomProps: ChartZoomHooks): ChartRenderer {
    return (Recharts) => {
        if (isPieChart(props.chartConfig)) {
            return <PieChartRenderer outerProps={props} config={props.chartConfig} Recharts={Recharts} />;
        }
        if (isBarChart(props.chartConfig)) {
            return <BarChartRenderer
                outerProps={props}
                config={props.chartConfig}
                containerRef={zoomProps.containerRef}
                Recharts={Recharts}
            />;
        }
        if (isScatterPlot(props.chartConfig)) {
            return <ScatterPlotRenderer outerProps={props} config={props.chartConfig} Recharts={Recharts} />;
        }
        if (isTimeSeries(props.chartConfig)) {
            return <TimeSeriesRenderer outerProps={props} config={props.chartConfig} zoomProps={zoomProps} Recharts={Recharts} />;
        }
        throw new Error(`Invalid chart type: ${props.chartConfig.chartType}`);
    };
}



