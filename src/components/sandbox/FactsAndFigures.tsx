import { ListResponse, RoundGroup } from 'picturegame-api-wrapper';
import * as React from 'react';

import { ApiContext } from '../../api/PicturegameApi';
import { RoundAggFieldTranslations, RoundFieldTranslations } from '../../model/Translation';
import { useZoomState } from '../../utils/ChartUtils';
import { makeCancellable } from '../../utils/PromiseUtils';

import { useEvent } from '../../hooks/Events';
import { useDocumentTitle } from '../../hooks/Misc';

import { Button } from '../base/Button';
import { CustomChart } from '../custom_charts/CustomChart';
import { ChartData, CustomChartType } from '../custom_charts/CustomChartHelper';

import { RoundAggFields, RoundGroupingFields } from '../custom_charts/ChartSpecs';

import { SandboxSpec } from './sandbox';

import './Sandbox.scss';

async function fetchSpec(): Promise<SandboxSpec> {
    const res = await fetch('fandf.json');
    if (res.status === 200) {
        return await res.json();
    }

    throw { status: res.status };
}

export default function FactsAndFiguresPage() {
    useDocumentTitle(() => 'Facts and Figures 2022', []);

    const api = React.useContext(ApiContext);

    const [spec, setSpec] = React.useState<SandboxSpec>();
    const [specFailed, setSpecFailed] = React.useState(false);

    React.useEffect(() => {
        const { promise, cancel } = makeCancellable(fetchSpec());
        promise
            .then(setSpec)
            .catch(err => {
                if (!err.cancelled) {
                    setSpecFailed(true);
                }
            });

        return cancel;
    }, []);

    const [data, setData] = React.useState<ChartData[][] | null>(null);
    React.useEffect(() => {
        if (data || !spec || !spec.requests) {
            return;
        }

        const { promise, cancel } = makeCancellable(api.bulkRequest({ requests: spec.requests }));
        promise.then(res => {
            setData(res.responses.map(((r: ListResponse<RoundGroup>) => r.results as unknown as ChartData[])));
        });
        return cancel;
    }, [data, api, spec]);

    const zoomProps = useZoomState();

    const [sectionsOpen, setSectionsOpen] = React.useState<boolean[]>([]);
    React.useEffect(() => {
        if (spec?.requests) {
            setSectionsOpen(spec.layout.map(() => true));
        }
    }, [spec]);
    function toggleSection(index: number) {
        const nextSections = [...sectionsOpen];
        nextSections[index] = !nextSections[index];
        setSectionsOpen(nextSections);
    }

    const [showExpandedWordCloud, setShowExpandedWordCloud] = React.useState(false);
    const onKeyPress = React.useCallback((ev: KeyboardEvent) => {
        if (ev.key === 'Escape') {
            setShowExpandedWordCloud(false);
        }
    }, []);
    useEvent('keydown', onKeyPress);

    if (specFailed) {
        return (
            <div className='page-viewport'>
                <div className='empty-state'>
                    <i className='icon far fa-hourglass' />
                    <div className='empty-state-message'>
                        This year's facts and figures are not ready yet! Check back later when we've finished working on them.
                    </div>
                </div>
            </div>
        );
    }

    if (!spec) {
        return (
            <div className='page-viewport'>
                <div className='loading-state'>
                    <div className='.loader' />
                </div>
            </div>
        );
    }

    return (
        <div className='page-viewport'>
            {showExpandedWordCloud && <div className='fullscreen-word-cloud'>
                <Button className='close-button' title='Close' onClick={() => setShowExpandedWordCloud(false)}>
                    <i className='fas fa-times' />
                </Button>
                <iframe src='https://picturegame.gitlab.io/picturegame-wordcloud' className='word-cloud-frame' />
            </div>}
            <div className='page-body'>
                {(spec?.layout ?? []).map((section, idx) => (
                    <SandboxSection
                        key={idx}
                        header={section.header}
                        isOpen={sectionsOpen[idx]}
                        toggleOpen={() => toggleSection(idx)}
                    >
                        <div className='sandbox-charts'>
                            {section.widgets.map((wid, i) => (wid.chart.chartType !== CustomChartType.WordCloud
                                ? <CustomChart
                                    key={i}
                                    data={data?.[wid.requestIndex] ?? []}
                                    loaded={!!data}
                                    chartConfig={wid.chart}
                                    groupingFieldTypes={RoundGroupingFields}
                                    groupingFieldTranslations={RoundFieldTranslations}
                                    metricFieldTypes={RoundAggFields}
                                    metricFieldTranslations={RoundAggFieldTranslations}
                                    {...zoomProps}
                                />
                                : <div className='chart-card double-width word-cloud-card' key={i}>
                                    <header>
                                        <div className='chart-header-text'>Round Titles</div>
                                        <div className='chart-header-buttons float-right'>
                                            <Button title='Expand' onClick={() => setShowExpandedWordCloud(true)}>
                                                <i className='fas fa-expand-alt' />
                                            </Button>
                                        </div>
                                    </header>
                                    <div className='word-cloud-cont'>
                                        <iframe src='https://picturegame.gitlab.io/picturegame-wordcloud' className='word-cloud-frame' />
                                    </div>
                                </div>))}
                        </div>
                    </SandboxSection>
                ))}
            </div>
        </div>
    );
}

interface SandboxSectionProps extends React.DOMAttributes<HTMLElement> {
    header: string;
    isOpen: boolean;
    toggleOpen(): void;
}

function SandboxSection(props: SandboxSectionProps) {
    return (
        <div className='sandbox-section'>
            <header onClick={props.toggleOpen}>
                <div className='section-header-text'>{props.header}</div>
                <div className='section-icon'>
                    <i className={'fas fa-caret-' + (props.isOpen ? 'up' : 'down')} />
                </div>
            </header>
            {props.isOpen && props.children}
        </div>
    );
}
