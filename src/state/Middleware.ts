import * as Redux from 'redux';

import * as UI from '../model/ui';

export const thunk: Redux.Middleware<{}, UI.State, UI.DispatchAction> = (store) => (next) => (action) => {
    if (typeof action === 'function') {
        return action(store.dispatch, store.getState);
    }
    return next(action);
};
