import * as UI from '../model/ui';

export function createActionCreator<A>(type: string): UI.ActionCreator<A> {
    const creator = ((args: A): UI.Action<A> => ({ args, type })) as UI.ActionCreator<A>;
    creator.type = type;

    return creator;
}
