import * as UI from '../model/ui';

export class Reducer<S> {
    private map = new Map<string, UI.Reducer<S, any>>();

    constructor(private initialState: S) {
    }

    handle<A>(action: UI.ActionCreator<A>, reducer: UI.Reducer<S, A>) {
        this.map.set(action.type, reducer);
    }

    getReducer(): UI.Reducer<S, UI.Action<any>> {
        return (state, action) => {
            if (state === undefined) {
                state = this.initialState;
            }
            const handler = this.map.get(action.type);
            if (handler) {
                return handler(state, action.args);
            }
            return state;
        };
    }
}
