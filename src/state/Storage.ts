import * as Redux from 'redux';

import * as UI from '../model/ui';

export interface SaveStateSpec<T> {
    extract: (state: UI.State) => T;
    unpack: UI.ActionCreator<T | null>;
}

export enum SaveStateKey {
    UserConfig = 'state.userConfig',
}

export class SaveStateMiddleware {
    private specs = new Map<SaveStateKey, SaveStateSpec<any>>();

    subscribe(dispatch: UI.DispatchAction) {
        window.addEventListener('storage', ev => {
            const spec = ev.key && this.specs.get(ev.key as SaveStateKey);
            if (spec) {
                const parsed = ev.newValue && JSON.parse(ev.newValue);
                dispatch(spec.unpack(parsed));
            }
        });
    }

    registerSaveState<T>(key: SaveStateKey, spec: SaveStateSpec<T>) {
        this.specs.set(key, spec);
    }

    middleware: Redux.Middleware<{}, UI.State, UI.DispatchAction> = store => next => action => {
        const prevState = store.getState();
        const result = next(action);
        const nextState = store.getState();

        for (const [key, { extract }] of this.specs) {
            const prevSubtree = extract(prevState);
            const nextSubtree = extract(nextState);

            if (prevSubtree !== nextSubtree) {
                trySaveState(key, nextSubtree);
            }
        }

        return result;
    }
}

function trySaveState(key: string, value: any) {
    const serialized = JSON.stringify(value);
    try {
        window.localStorage.setItem(key, serialized);
    } catch (e) {
        console.error(`Failed to save state for key ${key}`, e);
    }
}
