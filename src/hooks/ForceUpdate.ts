import * as React from 'react';

export function useForceUpdate() {
    const [, setValue] = React.useState(0);
    return () => setValue(v => v + 1);
}
