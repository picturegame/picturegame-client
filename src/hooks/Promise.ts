import * as _ from 'lodash-es';
import * as React from 'react';

import { makeCancellable } from '../utils/PromiseUtils';

export function useCancellableAsync<T>(promiseFn: () => Promise<T>, thenFn: (val: T) => void, deps: any[] = []) {
    React.useEffect(() => {
        const { promise, cancel } = makeCancellable(promiseFn());
        promise
            .then(thenFn)
            .catch(err => {
                if (err.cancelled) { return; }
                throw err;
            });
        return cancel;
    }, deps);
}

export function useAsyncData<TArgs extends any[], TReturn>(getData: (...args: TArgs) => Promise<TReturn>, ...args: TArgs) {
    const requestIdxRef = React.useRef(0);
    const [data, setData] = React.useState<TReturn>();
    const [requestPending, setRequestPending] = React.useState(false);

    const lastArgsRef = React.useRef<TArgs>();
    const currentArgsRef = React.useRef<TArgs>(args);

    const fetchAndUpdateData = React.useMemo(() => _.debounce(async () => {
        if (_.isEqual(lastArgsRef.current, currentArgsRef.current)) {
            return;
        }

        lastArgsRef.current = currentArgsRef.current;
        const requestIdx = ++requestIdxRef.current;
        setRequestPending(true);

        getData(...currentArgsRef.current).then(newData => {
            if (requestIdx === requestIdxRef.current) {
                setData(newData);
                setRequestPending(false);
            }
        });
    }, 500), [getData]);

    React.useEffect(() => {
        currentArgsRef.current = args;
        fetchAndUpdateData();
    }, [...args, getData]);

    // On unmount, increase the request idx one last time so that any in-flight requests will not try to update
    React.useEffect(() => {
        return () => {
            ++requestIdxRef.current;
        };
    }, []);

    return {
        data,
        requestPending,
    };
}
