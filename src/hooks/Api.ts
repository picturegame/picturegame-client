import * as React from 'react';

import { ApiContext, PicturegameApi } from '../api/PicturegameApi';

export class QueryContext<TParam, TArgs extends any[], TOut> {
    private pending = new Set<TParam>();
    private processing: TParam[] | null;

    constructor(
        private api: PicturegameApi,
        private fetch: (params: TParam[], api: PicturegameApi, ...args: TArgs) => Promise<TOut[]>,
        private onData: (params: TParam[], data: TOut[]) => void,
        private args: TArgs,
    ) { }

    public enqueue(params: TParam[]) {
        for (const param of params) {
            this.pending.add(param);
        }
        if (!this.processing) {
            this.process();
        }
    }

    public dequeue(params: TParam[]) {
        for (const param of params) {
            this.pending.delete(param);
        }
    }

    private async process() {
        if (!this.pending.size) { return; }

        this.processing = [...this.pending];
        const data = await this.fetch(this.processing, this.api, ...this.args);
        this.onData(this.processing, data);
        this.dequeue(this.processing);
        this.processing = null;

        if (this.pending.size) {
            this.process();
        }
    }
}

export function useApiQueryContext<TParam, TArgs extends any[], TOut>(
    fetch: (params: TParam[], api: PicturegameApi, ...args: TArgs) => Promise<TOut[]>,
    onData: (params: TParam[], data: TOut[]) => void,
    ...args: TArgs) {

    const api = React.useContext(ApiContext);
    return React.useMemo(() => new QueryContext(api, fetch, onData, args), [api, fetch, onData, ...args]);
}
