# 1.11

## Features

* Add yearly facts and figures page

## Improvements

* Improve UX for selecting colours for charts
* Improve zooming on charts

# 1.10

## Features

* Add custom date filter for the leaderboard/dashboards

## Improvements

* Move date filters to the main page so as to make them more discoverable
* Improve sorting of player search results to favour substring matches

# 1.9

## Features

* Add ability to sort the rounds explorer page

## Improvements

* Change the document title to something relevant to the page the user is viewing
* Filter abandoned rounds when calculating user's overview stats on the dashboard

# 1.8

## Features

* Add proper filtering UI to the rounds explorer page

## Improvements

* Change Player Dashboard to use the rounds view from the rounds explorer page instead of a grid

# 1.7

## Features

* Add rounds explorer page
    * This will be updated later to have a more user friendly filtering experience, but for now you can manually enter a filter using the same syntax as the API

## Improvements

* Improve UI layout for narrow mobile displays
* Allow drag-to-zoom to work when dragging off the edge of a chart

# 1.6

## Features

* Add ability to zoom in on the charts
* Add weekly wins chart (#28)
* Add ability to hide individual charts

# 1.5

## Features

* Add tooltips to the charts on the dashboard (#24)

# 1.4

## Features

* Add sidebar
* Add pre-canned global date range filters, accessible in the sidebar
* Add peak rank info to dashboard and comparison pages

## Improvements

* Improve the UX of the player search widget
    * Selecting from the dropdown automatically submits
    * Pressing enter submits
* Performance improvements

# 1.3

## Features

* Round and rank charts now use time as the x axis, rather than round number

# 1.2

## Features

* Comparison page - view high-level stats and rounds/rank charts for multiple players on the same page

## Bug Fixes

* Fixed an issue where rank changes would be recomputed every time new player aggregates were fetched

# 1.1

## Improvements

* Improved site layout on mobile devices

# 1.0

Initial stable release

## Features

* Paginated all-time leaderboard
* Per-user dashboard page containing:
    * High-level aggregates
    * Wins-over-time and rank-over-time charts
    * List of rounds won
