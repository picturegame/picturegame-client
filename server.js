'use strict'
const express = require('express');
const fs = require('fs');
const path = require('path');

const webpack = require('webpack');
const webpackMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const webpackConfig = require('./webpack.config');

const packageVersion = require('./package.json').version;

const port = 8558;
const host = '0.0.0.0';

var app = express();

const webpackCompiler = webpack(webpackConfig);

app.use(webpackMiddleware(webpackCompiler, {
    publicPath: webpackConfig.output.publicPath,
    stats: {
        colors: true,
    },
}));
app.use(webpackHotMiddleware(webpackCompiler, {
    log: console.log,
    heartbeat: 10 * 1000,
}));

app.use(express.static('./'));

app.get('/:file.json', (req, res) => {
    const filePath = path.join(__dirname, `${req.params.file}.json`);

    if (!fs.existsSync(filePath)) {
        res.status(404).json({ error: 'File not found' });
        return;
    }

    const fileContents = JSON.parse(fs.readFileSync(filePath, { encoding: 'utf8' }));
    res.json(fileContents);
});

app.get('/VERSION', (req, res) => {
    res.send(`${packageVersion}-dev`);
})

app.get('*', (req, res, next) => {
    var filename = path.join(webpackCompiler.outputPath, 'index.html');
    webpackCompiler.outputFileSystem.readFile(filename, (err, result) => {
        if (err) { return next(err) }
        res.set('content-type', 'text/html');
        res.send(result);
        res.end();
    });
});

app.listen(port, host);
