const path = require('path');
const webpack = require('webpack');

const CircularDependencyPlugin = require('circular-dependency-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const ExtractCssChunks = require('extract-css-chunks-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const HtmlPlugin = require('html-webpack-plugin');
const ScriptExtHtmlPlugin = require('script-ext-html-webpack-plugin');

module.exports = {
    entry: {
        index: './src/client',
    },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].[contenthash].js',
        chunkFilename: '[name].[contenthash].js',
        library: 'PictureGame',
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
    },
    plugins: [
        new CircularDependencyPlugin({
            exclude: /node_modules/,
            failOnError: true,
        }),
        new ExtractCssChunks({
            filename: '[name].[contenthash].css',
            chunkFilename: '[name].[contenthash].css',
        }),
        new OptimizeCssAssetsPlugin(),
        new CompressionPlugin(),
        new HtmlPlugin({
            template: 'index-template.html',
        }),
        new ScriptExtHtmlPlugin({
            custom: [
                {
                    test: /index.*\.js/,
                    attribute: 'onload',
                    value: 'window.PictureGame.load()',
                },
            ],
        }),
    ],
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
            },
            {
                test: /\.scss$/,
                use: [
                    { loader: ExtractCssChunks.loader },
                    { loader: 'css-loader' },
                    { loader: 'sass-loader' },
                ],
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [
                    'file-loader',
                    {
                        loader: 'image-webpack-loader',
                        options: {
                            pngquant: {
                                quality: '65-90',
                            },
                        },
                    },
                ],
            },
        ],
    },
    mode: 'production',
}
