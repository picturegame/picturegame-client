# picturegame-client

A front-end application for viewing information served by the [picturegame-api](https://gitlab.com/picturegame/picturegame-api), written in React.

## Configuration

Config is stored in config.json in the root directory of the project. Config keys are described in `src/model/ui.d.ts` in the `Config` interface.

## To run (dev)

* `yarn`
* `yarn start`

The dev server supports hot-reloading.

## To build (prod)

* `yarn`
* `yarn build`

The build process generates an `index.html` file, along with all javascript/css chunks, in the `dist` directory.

## Supported Browsers

* **Firefox** - I do all development and almost all testing in Firefox, so this will give the most stable experience
* **Chrome** - I test briefly in Chrome before releases, so you should be able to get away with using it

There are some known issues in **Edge** which I have no feasible way of, or interest in fixing.
If you insist on using this browser, you are welcome to try to fix any issues you come across and send me a pull request.
